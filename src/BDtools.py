# -*- coding: UTF-8 -*-
import cv2
import numpy as np
import scipy.fft as fftlib

from mpiAdapter import MPIAdapter


def fft2D(inp):
    '''
    2D FFT
    如果输入inp是2D complex矩阵，则进行2D FFT变换
    如果输入inp是3D complex矩阵，则对 中inp每个切片（axes=0）进行2D FFT，即对inp[ith,:,:]后两维进行FFT
    :param : inp输入2D或者3D矩阵，可以是real 类型或者 complex类型.
    :return: 如果inp是2D的，则返回2D complex矩阵；如果inp是3D的，则返回3D complex矩阵
    '''

    # Estimate the rough number of operations involved in the FFT
    # and discard the imaginary part if within roundoff error
    # roundoff error  = machine epsilon * number of operations
    out = fftlib.fft2(fftlib.fftshift(inp, axes=(-2, -1)))
    # n_ops = np.sum(inp.size * np.log2(inp.size))
    # out = np.real_if_close(out, tol=n_ops)
    height, width = inp.shape[-2:]
    nOps = width * height * np.log2(height) + height * width * np.log2(width)
    if abs(out.imag).max() / abs(out).max() <= nOps * np.finfo(float).eps:
        out = out.real
    return out


def ifft2D(inp):
    '''
    2D FFT
    如果输入inp是2D complex矩阵，则进行2D iFFT变换
    如果输入inp是3D complex矩阵，则对 中inp每个切片（axes=0）进行2D iFFT，即对inp[ith,:,:]后两维进行iFFT
    :param : inp输入2D或者3D矩阵，可以是real 类型或者 complex类型
    :return: 如果inp是2D的，则返回2D complex矩阵；如果inp是3D的，则返回3D complex矩阵
    '''
    out = fftlib.ifftshift(fftlib.ifft2(inp), axes=(-2, -1))
    # n_ops = np.sum(inp.size * np.log2(inp.size))
    # out = np.real_if_close(out, tol=n_ops)
    height, width = inp.shape[-2:]
    nOps = width * height * np.log2(height) + height * width * np.log2(width)
    if abs(out.imag).max() / abs(out).max() <= nOps * np.finfo(float).eps:
        out = out.real
    return out


def corr2D(A, B):
    '''
    对输入函数A，B，计算并返回cross correlation。当A=B时则返回auto correlation。
    采用基于 FFT算法的求相关方法,即。
    A@B= iFFT( conjugate( FFT(A) ) * FFT(B) ) , @为相关运算， * 为普通的元素乘法
    :param A: 输入2D 或 3D 张量， 对后两维做A[ith.:,:]相关运算，dtype=complex
    :param B: 输入2D 或 3D 张量， 对后两维做A[ith.:,:]相关运算, dtype=complex
    :return: 返回 A@B，2D或3D张量，dtype=complex
    '''

    KA = fft2D(A)
    KB = fft2D(B)
    return corr2D_K(KA, KB)


def corr2D_K(KA, KB):
    '''
    从K空间计算两个矩阵的相关，如果从K空间开始算，计算量约为实空间的1/3
    :param KA: FFT(A[ith,:,:]), dtype=complex
    :param KB: FFT(B[ith,:,:]), dtype=complex
    :return: A@B= iFFT( conjugate(KB) * KA)，一般不满足对易性，即 A@B!=B@A
    '''
    return ifft2D(np.conj(KB) * KA)


def conv2D(A, B):
    '''
    对输入图像数据A，B计算器卷积: A&B = iFFT( FFT(A) * FFT(B) )。
    :param A: 输入2D 或 3D 张量， 对后两维做A[ith.:,:]卷积运算，dtype=complex
    :param B: 输入2D 或 3D 张量， 对后两维做B[ith.:,:]卷积运算, dtype=complex
    :return: 返回 A&B，2D或3D张量，dtype=complex
    '''
    KA = fft2D(A)
    KB = fft2D(B)
    return conv2D_K(KA, KB)


def conv2D_K(KA, KB):
    '''
    对输入图像数据A，B计算器卷积: A&B = iFFT( FFT(A) * FFT(B) )。
    :param KA: KA=FFT(A)，2D 或 3D 张量, dtype=complex
    :param KB: KA=FFT(B):2D 或 3D 张量, dtype=complex,
    :return: 返回 A&B，2D或3D张量，dtype=complex
    '''
    return ifft2D(KA * KB)


if __name__ == '__main__':
    '''
    test this py
    '''
    import sys
    import time

    import matplotlib.pyplot as plt
    from scipy import signal

    from globalvar import GlobalVar
    from Img import Img

    # 读入图像数据
    par = GlobalVar()
    img = Img(par)

    # 读入图像
    imgData = img.read_image_data()
    ith = 0
    inp = np.zeros(shape=(imgData.shape[1], imgData.shape[2]), dtype=float)
    inp2 = np.zeros(shape=(imgData.shape[1], imgData.shape[2]), dtype=float)

    inp[:, :] = imgData[ith, :, :]
    inp2[100:120, 100:120] = 1.0
    inp2 = inp

    '''
    # 测试运行时间
    start = time.time()
    for nt in range(0,1000):
        F = fft2D(imgData)
        f = ifft2D(F)
        conv2D(f,f)
        corr2D(f,f)
    end = time.time()
    print('running time {} s'.format(end-start))
    quit()
    '''
    o1 = cv2.dft(imgData[0, :, :], cv2.DFT_SCALE)
    print(np.mean(o1))

    out = fft2D(imgData[0, :, :])
    print('mean: ', np.mean(imgData[0, :, :]), ', ', np.mean(out))

    '''
    # 测试和比较相关程序， 对比 ‘scipy.signal.correcte2D’ #
    print('... Test Correlate2D ...')
    co1 = signal.correlate2d(inp, inp2, mode='same')
    plt.suptitle('两个量的相关测试')
    plt.subplot(2, 2, 1)
    plt.imshow(np.abs(co1), cmap='gray')
    plt.title('real part of sci.signal.corr2D')
    plt.colorbar()

    plt.subplot(2, 2, 2)
    plt.imshow(np.imag(co1), cmap='gray')
    plt.title('imag part of sci.signal.corr2D')
    plt.colorbar()

    co2 = corr2D(inp, inp2)
    plt.subplot(2, 2, 3)
    plt.imshow(np.abs(co2), cmap='gray')
    plt.title('real part of bdtool.corr2D')
    plt.colorbar()
    print('BDtool.corr2D done!')

    plt.subplot(2, 2, 4)
    plt.imshow(np.imag(co2), cmap='gray')
    plt.title('imag part of bdtool.corr2D')
    plt.colorbar()

    plt.show()
    print('BDtool.corr2D done!')

    quit()

    # 测试和比较卷积程序程序， 对比 ‘scipy.signal.conv2D’ #
    print('... Test Convolution2D ...')
    con1 = signal.convolve2d(inp, inp2, boundary='symm', mode='same')
    ax1 = plt.subplot(1, 2, 1)
    tu1 = plt.imshow(np.abs(con1), cmap='gray')
    plt.title('convolution2D from scipy.signal.convolve2d')
    plt.colorbar(tu1)


    con2 = conv2D(inp, inp2)
    ax2 = plt.subplot(1, 2, 2)
    tu2 = plt.imshow(np.abs(con2), cmap='gray')
    plt.title('convolution2D from this conv2D')
    plt.colorbar(tu2)

    plt.show()
    quit()

    # quit()
    # 测试图像通过FFT和iFFT后能否复原，虚部是否为0
    '''

    '''
    plt.subplot(1, 4, 1)
    plt.imshow(inp, interpolation='nearest', cmap='bone')
    plt.colorbar()

    d = np.zeros(shape=(imgData.shape[1], imgData.shape[2]), dtype=float)
    d[:, :] = np.log2(np.abs(F[ith, :, :]))
    plt.subplot(1,4,2)
    plt.imshow(d, interpolation='nearest', cmap='bone')
    plt.colorbar()

    d[:, :] = np.real(f[ith, :, :])
    plt.subplot(1,4,3)
    plt.imshow(d, interpolation='nearest', cmap='bone')
    plt.colorbar()

    d[:, :] = np.abs(np.imag(f[ith, :, :]))
    plt.subplot(1, 4, 4)
    plt.imshow(d, interpolation='nearest', cmap='bone')
    plt.colorbar()

    plt.show()# 
    '''
