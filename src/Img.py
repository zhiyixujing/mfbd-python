# -*- coding: UTF-8 -*-
'''
read img from specific path
'''

import os
import re
import sys

import cv2
import fitsio
import imageio
import matplotlib.pyplot as plt
import numpy as np
from scipy.fft import fftshift

from BDtools import fft2D, ifft2D
from globalvar import GlobalVar
from mpiAdapter import MPIAdapter


class Img:
    """
    Bmp or FITS image
    """

    mpiadapter = MPIAdapter()
    pfft = None

    def __init__(self, _par: GlobalVar, filenames=[]):
        self.par = _par

        # 读入输入参数，’ImgFilePath‘,'ImgFileFormat'
        self.path = self.par.getString('ImgFilePath')
        self.format = self.par.getString('ImgFileFormat').lower()
        mpiadapter = self.par('mpiadapter')
        self.rank = mpiadapter.rank()

        # 获得输入文件名列表，并进行排序
        allFilelist = self.get_filenames() if len(filenames) == 0 else filenames
        allFilelist.sort(key=self.digit_sort)

        # 获取并行计算时每个进程负责的图片列表
        sampleImg = self.loadAImg(allFilelist[0])
        self.height = sampleImg.shape[0]
        self.width = sampleImg.shape[1]
        self.par.set("height", self.height)
        self.par.set("width", self.width)
        self.filelist = []
        for i in self.mpiadapter.distribute(len(allFilelist)):
            self.filelist.append(allFilelist[i])
        self.nImg = len(self.filelist)

        # 添加带限矩阵
        if self.par.getString("BDhParaMethod").lower() in ['hnonnegative_bandlimited', 'hnonnegative_normalization_bandlimited']:
            ccdu = self.par.getFloat('ccdu')
            ccdN = self.par.getFloat('ccdN')
            f0 = self.par.getFloat('f0')
            D0 = self.par.getFloat('D0')
            minlam = self.par.getFloat('minlambda')
            self.par.set("fstop", round(ccdN * ccdu * D0 / minlam / f0))
            P, S = self.par.PupilMask(self.par.getFloat("fstop"), (len(self.filelist), self.height, self.width))
            bl = abs(fftshift(fft2D(P), axes=(-2, -1)))**2 / (S * P.shape[-2] * P.shape[-1])
            self.par.set("Bl", fft2D(bl))

    def checkImgExist(self, imgPath):
        '''
        check if img is exist
        '''
        if not os.path.exists(imgPath):
            print('图像 ', imgPath, ' 不存在，程序将退出')
            sys.exit()

    def loadAImg(self, imgPath):
        '''
        load A img from file
        '''
        if self.format == "bmp":
            return cv2.imread(imgPath, flags=cv2.IMREAD_GRAYSCALE)
        else:
            return fitsio.read(imgPath)

    def get_filenames(self):
        '''
        :param path: 搜索文件的目录名，在该目录下找出所有后缀为 '.fits .bmp'的文件
        :return: 找到的文件列表
        '''
        rootpath = '{0}{1}'.format(self.path, '/')
        abspath = os.path.abspath(rootpath)
        allfiles = os.listdir(abspath)

        outfile = []
        for file in allfiles:
            absfile = os.path.join(abspath, file)

            # 判断是不是文件
            if os.path.isfile(absfile):
                # 判断后缀是不是指定的格式
                ext = os.path.splitext(absfile)[-1]
                ext0 = '.' + self.format
                if ext == ext0:
                    outfile.append(absfile)
        return outfile

    def digit_sort(self, mystr):
        '''
        根据输入的字符串中部分(文件名basename)中的数字的升序进行排列
        :param my_list: 输入的字符串list
        :return: 输出排序完成后的字符串
        '''
        # 去掉目录
        s0 = os.path.split(mystr)[1]
        # 去掉后缀名
        s1 = os.path.splitext(s0)[0]
        # 分离尾部数字部分，如果没有，则为0
        s2 = re.findall(r'[^\D]+$', s1)

        s3 = []
        for s in s2:
            if s.isdigit():
                s3.append(int(s))
            else:
                s3.append(0)

        return s3

    def read_image_data(self):
        '''
        read image data from file list
        :param filelist:
        :return: image data from a batch of files
        '''

        imgdata = np.zeros([self.nImg, self.height, self.width])

        for ith in range(len(self.filelist)):
            curImg = self.filelist[ith]
            self.checkImgExist(curImg)
            data = self.loadAImg(curImg)
            # 判断每个图像文件是不是大小相同
            assert self.height == data.shape[0]
            assert self.width == data.shape[1]
            print('进程 {}: 读入图像 {}'.format(self.rank, curImg))

            # 检查是否存在 < 0 的值
            imgdata[ith] = np.where(data < 0, 1e-14, data)

        return imgdata

    def saveImg_f(self, myf, nIter):
        '''
        保存迭代过程中的真实图像文件
        :param myf: 2D, 不同参数表示不同的图像
        :param nIter: 第 niter 个迭代步
        :return: 无
        '''

        if not Img.mpiadapter.isMaster():
            return

        par = self.par
        if par.getString("BDfParaMethod").lower() == 'fnonnegative':
            f = np.abs(myf * myf)
        else:
            f = np.abs(myf)

        # 将图像矩阵A中介于[amin,amax]的数据归一化处理，小于amin的元素变为amin，大于amax的元素变为amax，并归一化到[0,255]之间用于结果保存为bmp
        rst = np.zeros_like(f)
        fsort = np.sort(f.flatten())
        amin = fsort[int(np.floor(np.size(fsort) * 0.05))] * 0.95
        amax = fsort[int(np.floor(np.size(fsort) * 0.999))] * 1.03
        fclip = np.clip(f, amin, amax)
        # cv2.normalize(src=f, dst=rst, alpha=amin, beta=amax, norm_type = cv2.NORM_MINMAX)
        cv2.normalize(src=fclip, dst=rst, alpha=0, beta=255.0, norm_type=cv2.NORM_MINMAX)
        cv2.imwrite("{}/f{:05d}.bmp".format(self.par.getString("ResultFilePath"), nIter), rst)
        fitsio.write("{}/f{:05d}.fits".format(self.par.getString("ResultFilePath"), nIter), f)

        return rst.astype('uint8')

    def saveImg_h(self, myh, nIter):
        '''
        保存迭代过程中的 PSF 函数 h
        :param myh: 输入的3D PSF，如果是多帧图像，只保存第一帧，即 h[ith,:,:]
        :param nIter: 第 niter 个迭代步
        :return: 无
        '''
        if not Img.mpiadapter.isMaster():
            return
        method = self.par.getString("BDhParaMethod").lower()
        if method == 'hnonnegative_normalization':
            sita = np.abs(myh)**2
            ss = np.sum(sita, axis=(-2, -1))
            h = (sita.T / ss).T
        elif method == 'hnonnegative':
            h = np.abs(myh) ** 2
        elif method == 'hnonnegative_bandlimited':
            sita = np.abs(myh)**2
            H = fft2D(sita) * self.par.get("Bl")
            h = ifft2D(H)
        elif method == 'hnonnegative_normalization_bandlimited':
            sita = np.abs(myh)**2
            ss = np.sum(sita, axis=(-2, -1))
            h = (sita.T / ss).T
            H = fft2D(sita) * self.par.get("Bl")
            h = ifft2D(H)
        else:
            h = np.abs(myh)

        im = np.zeros(shape=(h.shape[1], h.shape[2]), dtype=float)
        rst = np.zeros_like(im)
        im[:, :] = np.abs(h[0, :, :])
        cv2.normalize(src=im, dst=rst, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX)
        cv2.imwrite("{}/h{:05d}.bmp".format(self.par.getString("ResultFilePath"), nIter), rst)
        fitsio.write("{}/h{:05d}.fits".format(self.par.getString("ResultFilePath"), nIter), im)

        return rst.astype('uint8')

    @staticmethod
    def composeGif(frameList, fileName, duration):
        if not Img.mpiadapter.isMaster():
            return

        imageio.mimsave(fileName, frameList, 'GIF', duration=duration)

    @staticmethod
    def save(data, path):
        cv2.imwrite(path, data)
        pass


if __name__ == '__main__':
    '''
    test this py
    '''

    par = GlobalVar()
    img = Img(par)
    print(img.path, img.format)

    str0 = 'c:/dd09-298/sf23ddxxf23.fits'
    print(str0)
    print('test digit_sort', img.digit_sort(str0))

    mylist = ['pyt23hon3', 'c:/fft3/fu28.inp', 'lea14rn', 'co.00l', 'c:/dd2/ob8uje3345t', './dd2/obaduj56.bmp']
    mylist.sort(key=img.digit_sort)

    file_list = img.get_filenames()
    file_list.sort(key=img.digit_sort)
    for name in file_list:
        print(name)

    # 从输入文件中读取图像数据
    imgData = img.read_image_data()
    data = np.zeros(shape=(img.height, img.width), dtype=imgData.dtype)
    data[:, :] = imgData[0, :, :]
    print(GlobalVar.global_dict)
    print('size and type of np.imgData', imgData.shape, imgData.dtype)
    # plt.imshow(data,interpolation = 'nearest', cmap = 'bone')
    # plt.show()

    # 将imgData保存，再次读取，并画图
    img.saveImg_h(imgData, 0)  # f<--g #
    fname = '{}/h{:05d}.bmp'.format(img.par.getString("ResultFilePath"), 0)
    print(fname)
    dat = cv2.imread(fname, flags=cv2.IMREAD_GRAYSCALE)
    plt.imshow(dat, cmap='bone')
    plt.colorbar()
    plt.show()
