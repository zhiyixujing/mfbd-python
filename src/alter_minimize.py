import datetime
import sys

import numpy as np

from fidelity import Fidelity
from globalvar import GlobalVar
from Img import Img
from initialization import Initialization
from line_search import DBrent, brent
from objfunction import ObjFun
from regularization import Regularization


class AlterMinimize:

    """
    交错迭代方法最小化代价函数
    """

    def __init__(self, _par, _img, _init, _obj):
        self.par = _par     # 控制输入参数的类
        self.img = _img     # 控制图像输入、输出的类
        self.init = _init    # 初始化类
        self.obj = _obj     # 控制代价函数的类
        self.mpiadapter = self.par('mpiadapter')
        self.rank = self.mpiadapter.rank()

        # 获得 F,H的迭代步数
        FIterNum, HIterNum = self.par.getIterNumFH()
        self.FIterNum = FIterNum
        self.HIterNum = HIterNum
        self.BDMaxIter = _par.getInt("BDMaxIter")

    def alterMin(self, g, myf, myh):
        '''
        交错迭代方法最小化代价函数
        :param g:    观察图像, 3D
        :param myf:  真实图像, 2D
        :param myh:   PSF, 3D
        :return:
        '''

        MaxIter = int(self.par.getInt("BDMaxIter"))
        maxIterH = self.HIterNum
        maxIterF = self.FIterNum

        tic = datetime.datetime.now()

        fFrames = []
        hFrames = []
        niter = 0
        newh = myh
        newf = myf
        getfh_flag = 0
        while (niter < MaxIter):

            # 保存迭代过程中数据
            fFrames.append(self.img.saveImg_f(newf, niter))
            hFrames.append(self.img.saveImg_h(newh, niter))

            if (niter == 0) & ('hnonnegative' in self.par.get('BDhParaMethod').lower()):
                getfh_flag = 1
            getfh_flag = 0

            # Iterations of h
            self.obj.isRealImage = 0
            newh = self.fmin_CG(self.Jh, self.Jhprime, x0=newh, x1=g, x2=newf, tol=1.0e-6, maxiter=maxIterH[niter], getfh_flag=getfh_flag)

            # Iterations of f
            self.obj.isRealImage = 1
            newf = self.fmin_CG(self.Jf, self.Jfprime, x0=newf, x1=g, x2=newh, tol=1.0e-6, maxiter=maxIterF[niter], getfh_flag=getfh_flag)

            print("进程 {}: 迭代{:3d} 步，共耗时 {:5.2f}s, 完成{:4.1f}%".format(self.rank, niter + 1, (datetime.datetime.now() - tic).seconds, round((niter + 1) / MaxIter * 100)))
            sys.stdout.flush()

            niter = niter + 1

        # 保存最后一次迭代数据
        fFrames.append(self.img.saveImg_f(newf, niter))
        hFrames.append(self.img.saveImg_h(newh, niter))

        # 将所有中间图融合成 Gif 动图
        Img.composeGif(fFrames, "{}/f.gif".format(self.par.getString("ResultFilePath")), 0.2)
        Img.composeGif(hFrames, "{}/h.gif".format(self.par.getString("ResultFilePath")), 0.2)

        return None

    # 代价函数 及其导数： Jf, Jh, Jfprime, Jhprime
    # 需要优化的参数 x 必须是函数的第一个参数

    def Jf(self, x, g, mh, getfh_flag=0):
        return self.obj.Jall(g, x, mh, getfh_flag)

    def Jfprime(self, x, g, mh, getfh_flag=0):
        return self.obj.Jall_df(g, x, mh, getfh_flag)

    def Jh(self, x, g, mf, getfh_flag=0):
        return self.obj.Jall(g, mf, x, getfh_flag)

    def Jhprime(self, x, g, mf, getfh_flag=0):
        return self.obj.Jall_dh(g, mf, x, getfh_flag)
    #############################################

    def fmin_CG(self, f, fprime, x0, x1, x2, tol=1.0e-5, maxiter=10, getfh_flag=0):
        '''
        非线性的 CG 求解函数 f(x) 局部最小值算法,
        与线性的 CG 方法不同的是，非线性 CG 需要知道沿某个方向的搜索长度，
        这里采用 line_search.py 中 Brent搜索算法。

        parameters:
        input f:          优化函数
        input fprime:     f的一阶导数
        input dx0:        nD float np.array， 搜索方向
        input x1:         nD float np.narray, f的第二个输入参数
        input x2:         nD float np.narray, f的第三个输入参数
        input a,b         float, 初始猜测值，optional
        input tol:        float, 容差
        input maxiter:    最大迭代步数

        outputs: xmin:    最小化 f 后的 x位置
        outputs: fmin:    优化后最小的 f 值
        '''

        niter = 0
        EPS = np.finfo(float).eps

        x = x0  # 待优化的变量 np.narray
        gk = fprime(x, x1, x2, getfh_flag)  # 梯度方向
        fk = f(x, x1, x2, getfh_flag)  # 初始函数值
        dk = -gk  # 搜索方向
        gk_1 = gk
        dk_1 = dk

        while (niter < maxiter):

            # 梯度很小时，直接退出
            if np.sqrt(np.mean(gk * gk)) < EPS * np.size(x0):
                break

            # 搜寻沿 dk 方向的移动距离
            a, fmin = brent(f, x, dk, x1, x2, tol, maxiter=100)  # 无导数计算，更快
            # a, fmin = DBrent(f, fprime, x, dk, x1, x2, tol, maxiter=100) # 有导数计算

            # 根据 a更新位置 x(k+1), x(k+1) = x(k) + a(k) * d(k)
            x = x + max(a, 0) * dk

            # 计算当前梯度方向
            gk = fprime(x, x1, x2)

            # 更新共轭梯度方向
            bb = gk_1 * gk_1               # gk_1 为上一个迭代步方向
            dbb = (gk - gk_1) * gk         # Polak-Ribier (PRP) 比 Fletcher-Reeves (FR)方法收敛更快
            b = np.sum(dbb) / np.sum(bb)
            dk = -gk + max(b, 0) * dk_1    # 根据b(k-1)更新d(k)

            # 收敛条件
            if 2 * np.abs(fmin - fk) <= tol * (np.abs(fmin) + np.abs(fk) + EPS):
                break

            # 更新循环变量

            gk_1 = gk
            dk_1 = dk
            fk = fmin
            niter = niter + 1

        # 返回最小值所在位置
        return x


################################################
if __name__ == '__main__':
    '''
    test this class
    '''

    par = GlobalVar()
    img = Img(par)
    imgData = img.read_image_data()
    init = Initialization(par, img)
    g = init.init_g(imgData)
    f = init.init_f(par, imgData)
    h = init.init_h(par, imgData)
    myf, myh = init.init_input(g)

    fide = Fidelity(par, g)
    regu = Regularization(par)

    obj = ObjFun(par, regu, fide)
    alter = AlterMinimize(par, img, init, obj)

    # #### 测试 brent() #####
    x0 = myh
    dx0 = -alter.Jhprime(myh, g, myf)
    xout, fout = brent(alter.Jh, x0, dx0, x1=g, x2=myf)
    print(alter.Jh(x0, g, myf), fout)

    '''
    ##### 测试 fmin_CG() #####
    tm_start = datetime.datetime.now()
    newh = alter.fmin_CG(alter.Jh, alter.Jhprime, myh, x1=g, x2=myf, tol=1.0e-5)
    #newf = alter.fmin_CG(alter.Jf, alter.Jfprime, myf, x1=g, x2=newh, tol=1.0e-5)
    tm_end = datetime.datetime.now()
    print('Running Time of fmin_CG= ', tm_end-tm_start)
    '''

    # #### 测试 alterMin() ####
    tm_start = datetime.datetime.now()
    alter.alterMin(g, myf, myh)
    tm_end = datetime.datetime.now()
    print('Running Time of alterMin ', tm_end - tm_start)
