# fft_test 不是整个软件的一部分， 它的目的是测试 FFT相关的算法，包括：
# 1. rfft 和 fft 的运行速度比较
# 2. sci.fft 和 FFTW 速度比较
# 3. 数据为 float 和 double 的差异
# 4. pfft， mpipy-fft包的使用方法
# 5. fftw.plan 的 ESTIMATE, MEASURE 的差异
# ------------------------------------------------------------------

import numpy as np
from mpi4py import MPI
from mpi4py_fft import PFFT, newDistArray
# from mpi4py_fft.fftw import dctn, idctn
from mpi4py_fft.pencil import Subcomm

# import pyfftw

# from globalvar import GlobalVar
# from Img import Img
# from initialization import Initialization


# def read_global_data():
#     N = 256   # 2D 数据的行数和列数，这里假设图像的行数和列数相同

#     img_old = cv.imread('lena.png',flags=cv.IMREAD_GRAYSCALE)
#     img_new = cv.resize(img_old, (N,N))

#     return img_new


# #----------------------------------------------------
# def numpyfft32(inp):
#     u0  = np.asarray(inp, dtype=np.complex64)
#     u1  = np.asarray(inp, dtype=np.complex64)

#     u0[:].real = inp
#     u0[:].imag = 0
#     u1[:]      = 0

#     start = time.time()
#     for i in range(0,100):
#         u1 = np.fft.fft2(u0)
#         u0 = np.fft.ifft2(u1)

#     end = time.time()
#     return end-start

# #----------------------------------------------------
# def numpyfft64(inp):
#     u0  = np.asarray(inp, dtype=np.complex128)
#     u1  = np.asarray(inp, dtype=np.complex128)

#     u0[:].real = inp
#     u0[:].imag = 0
#     u1[:]      = 0

#     start = time.time()
#     for i in range(0,100):
#         u1 = np.fft.fft2(u0)
#         u0 = np.fft.ifft2(u1)

#     end = time.time()
#     return end-start


# #----------------------------------------------------
# def scipyfft32(inp):
#     u0  = np.asarray(inp, dtype=np.complex64)
#     u1  = np.asarray(inp, dtype=np.complex64)

#     u0[:].real = inp
#     u0[:].imag = 0
#     u1[:]      = 0

#     start = time.time()
#     for i in range(0,100):
#         u1 = scipy.fft.fft2(u0)
#         u0 = scipy.fft.ifft2(u1)

#     end = time.time()
#     return end-start

# #----------------------------------------------------
# def scipyfft64(inp):
#     u0  = np.asarray(inp, dtype=np.complex128)
#     u1  = np.asarray(inp, dtype=np.complex128)

#     u0[:].real = inp
#     u0[:].imag = 0
#     u1[:]      = 0

#     start = time.time()
#     for i in range(0,100):
#         u1 = scipy.fft.fft2(u0)
#         u0 = scipy.fft.ifft2(u1)

#     #print(u1.size)
#     end = time.time()
#     return end-start

# #----------------------------------------------------
# def scipyfftr64(inp):
#     u0  = np.asarray(inp, dtype=np.float64)
#     u1  = np.asarray(inp, dtype=np.complex128)

#     u0[:] = inp
#     u1[:] = 0

#     start = time.time()
#     for i in range(0,100):
#         u1 = scipy.fft.rfft2(u0)
#         u0 = scipy.fft.irfft2(u1)

#     #print(u1.size)
#     end = time.time()
#     return end-start

# #----------------------------------------------------
# def pyfftw32(inp):
#     pyfftw.config.NUM_THREADS = 4
#     pyfftw.config.PLANNER_EFFORT = 'FFTW_MEASURE'
#     pyfftw.interfaces.cache.enable()
#     pyfftw.interfaces.cache.set_keepalive_time(30)

#     u0 = pyfftw.empty_aligned(inp.shape, dtype='float32')
#     u1 = pyfftw.empty_aligned(inp.shape, dtype='complex64')

#     u0[:] = inp
#     u1[:] = 0 + 0j

#     fft_plan  = pyfftw.builders.rfft2(u0)
#     ifft_plan = pyfftw.builders.irfft2(u1)

#     start = time.time()
#     for i in range(0,100):
#         u1= fft_plan()
#         u0= ifft_plan()

#     end = time.time()
#     return end-start

# #----------------------------------------------------
# def pyfftw64(inp):
#     pyfftw.config.NUM_THREADS = 4
#     pyfftw.config.PLANNER_EFFORT = 'FFTW_PATIENT'
#     pyfftw.interfaces.cache.enable()
#     pyfftw.interfaces.cache.set_keepalive_time(30)

#     u0 = pyfftw.empty_aligned(inp.shape, dtype='float64')
#     u1 = pyfftw.empty_aligned(inp.shape, dtype='complex128')

#     u0[:] = inp + 0j
#     u1[:] = 0 + 0j

#     fft_plan  = pyfftw.builders.fft2(u0)
#     ifft_plan = pyfftw.builders.ifft2(u1)

#     start = time.time()
#     for i in range(0,100):
#         u1= fft_plan()
#         u0= ifft_plan()

#     end = time.time()
#     return end-start

# 0, 1, 2, 3, 4, 5, 6, 7
# task: 0, 1, 2
# 1: 3, 4
# 2: 5, 6, 7
def mpifft():
    comm = MPI.COMM_WORLD
    N = np.array([32, 256, 256], dtype=int)
    fft = PFFT(comm, N, axes=(0, 1, 2), dtype=float, grid=(-1,))
    u = newDistArray(fft, False)
    u[:] = np.random.random(u.shape).astype(u.dtype)
    u_hat = fft.forward(u, normalize=True)  # Note that normalize=True is default and can be omitted
    uj = np.zeros_like(u)
    uj = fft.backward(u_hat, uj)
    assert np.allclose(uj, u)
    print(comm.Get_rank(), u.shape, uj.shape, u_hat.shape)



from scipy import fft, signal

if __name__ == '__main__':

    # inp        = read_global_data()
    # n1,n2      = 256, 256

    # tm1 = numpyfft32(inp)
    # tm2 = numpyfft64(inp)
    # tm3 = scipyfft32(inp)
    # tm4 = scipyfft64(inp)
    # tm5 = scipyfftr64(inp)
    # tm6 = pyfftw32(inp)
    # tm7 = pyfftw64(inp)

    # print( 'numpy32= {:.3f}, numpy64= {:.3f}, scipy32= {:3f}, scipy64= {:.3f}, scipyr64= {:.3f}, fftw32= {:.3f}, fftw64= {:.3f}'.format(tm1, tm2, tm3, tm4, tm5, tm6, tm7) )

    # par = GlobalVar()
    # img = Img(par)
    # tmp8 = mpifft(img.read_image_data())
    # mpifft()
    
    # comm = MPI.COMM_WORLD
    # subcomms = Subcomm(comm, [0, 0, 1])
    # if comm.Get_rank() == 1:
    #     for subcomm in subcomms:
    #         print(subcomm.Get_size())

    # if comm.Get_rank() > 2:
    #     curCom = subcomms[comm.Get_rank()-3]
    #     for i in range(curCom.Get_size()):
    #         print('Global:{}, subCom:{}, subRank:{}'.format(comm.Get_rank(), i, curCom.Get_rank()))
