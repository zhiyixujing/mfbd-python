import matplotlib.pyplot as plt
import numpy as np

from BDtools import conv2D_K, corr2D, corr2D_K, fft2D
from globalvar import GlobalVar
from Img import Img
from initialization import Initialization
from mpiAdapter import MPIAdapter


class Fidelity:
    """
    处理保真项及其导数的类
    在每次迭代过程中，需要计算 Jfid, dJfid/df, dJfid/dh，三次计算需要用到 g-conv2D(f，h), 我们将此共同的计算部分 放置在updateCommon 方法中处理以减少计算量，在计算保真项及其导数项前更新此变量，
    """

    def __init__(self, _par, _g):
        '''

        :param _par:   # 控制输入参数的类
        :param _g:     # 观察图像，g 不随迭代而改变
        '''

        self.par = _par
        self.g = _g
        self.mpiadapter = self.par('mpiadapter')

        # 设置噪声混合模型参数，MixnoiseCoef, 它只是 观察图像 g 的函数，在迭代过程中不变
        # MixnoiseCoef 是 3D dtype=float 矩阵
        # 避免 MixnoiseCoef == 0， 加上一个很小的值eps
        self.MixnoiseCoef = np.ones(shape=(_g.shape[0], _g.shape[1], _g.shape[2]), dtype=float)
        eps = np.finfo(np.float32).eps
        gabs = np.abs(_g)
        if self.par.getString("BDMixnoiseStat").lower() == 'true':
            ccdn2 = float(self.par.getInt("ccdnread"))**2
            tmp1 = MPIAdapter.avage(np.mean(gabs + ccdn2))
            tmp2 = gabs + ccdn2 + eps
            for ith in range(0, _g.shape[0]):
                self.MixnoiseCoef[ith, :, :] = tmp1 / tmp2[ith, :, :]
        else:
            self.MixnoiseCoef = 1.0

    def updateCommon(self, g, f, h):
        '''
        更新变量 common， 保留3D数据，用以存放 g-conv2D(f，h), 需要每次计算 Fidelity时更新
        common = g-f*h
        :return: self.common, 它是3D 复数
        '''
        F = fft2D(f)
        H = fft2D(h)
        H2D = np.zeros_like(F)
        comm = np.zeros_like(g)
        for ith in range(0, g.shape[0]):
            H2D[:, :] = H[ith, :, :]
            comm[ith, :, :] = g[ith, :, :] - conv2D_K(F, H2D)
        return comm

    def Jfid(self, g, f, h):
        '''
        保真项代价函数, 确保调用此函数前 self.common 已经更新过
        :param: g,f,h分别表示观察图像、真实图像、PSF图像，其中 f.ndim=2, g.ndim=3, h.ndim=3
        :return: 标量，保真项对应的代价函数
        '''
        comm = self.updateCommon(g, f, h)
        return MPIAdapter.avage(np.mean(np.power(comm, 2) * self.MixnoiseCoef))

    # ################### Jfid 对 f, phi 的导数  ######################
    def Jfid_df(self, g, f, h):
        '''
        保真约束的导数项 dJ/df, J是损失函数，为标量，f是2D矢量，因此 dJ/df 为2维矢量
        :param g: 观察图像，3D
        :param f: 真实图像， 2D
        :param h: PSF，3D
        :return: 2D，将所有图像的相加，并返回实数
        '''

        comm = self.updateCommon(g, f, h)
        dj3D = corr2D(-2.0 * self.MixnoiseCoef * comm, h)
        dj2D = np.sum(dj3D, axis=0)
        return MPIAdapter.sum(dj2D)

    def Jfid_dphi(self, g, f, h, phi):
        '''
        保真项的导数 d{J}/d{phi}, 其中 f= phi^2, 则 dJ/dphi= dJ/df *(2*phi)
        :param g: 观察图像，3D
        :param f: 真实图像， 2D
        :param h: PSF，3D
        :return: 2D，将所有图像的相加
        '''

        return self.Jfid_df(g, f, h) * (2 * phi)

    # ################### Jfid 对 h, theta, theta_norm 的导数  ######################
    def Jfid_dh(self, g, f, h):
        '''
        保真约束的导数项 dJ/dh， 因为 h 是 3D 矢量，因此 dJ/dh 结果为3D 矢量
        :param g: 观察图像，3D
        :param f: 真实图像， 2D
        :param h: PSF，3D
        :return: dJ/dh， 3D
        '''

        # 将 f 扩展成 3D
        comm = self.updateCommon(g, f, h)
        ff = np.tile(f, (g.shape[0], 1, 1))
        j3d = corr2D(-2.0 * self.MixnoiseCoef * comm, ff)
        return j3d

    def Jfid_dtheta(self, g, f, h, theta):
        '''
        保真项的导数 d{J}/d{theta}, 其中 h= theta^2, 则 dJ/d theta= dJ/dh *(2*theta)
        :param g: 观察图像，3D
        :param f: 真实图像， 2D
        :param h: PSF，3D
        :return: dJ/dtheta， 3D
        '''

        return self.Jfid_dh(g, f, h) * (2 * theta)

    def Jfid_dtheta_norm(self, g, f, h, theta_norm):
        '''
        保真项的导数 d{J}/d{theta}, 其中 h= theta^2/sum(theta^2)
        注意：此处无法采用 h 作为输入参数， 因为无法从 h中得到 theta，而只能从 theta 得到 h
        :param g:             观察图像，3D
        :param f:             真实图像， 2D
        :param theta_norm:     h= theta_norm^2/sum(theta_norm)， 3D
        :return: dJ/dtheta，   3D
        '''

        djdh = self.Jfid_dh(g, f, h)
        ss = np.tile(np.sum(theta_norm**2, axis=(1, 2)),
                     (h.shape[1], h.shape[2], 1)).transpose()
        sss = np.tile(np.sum(djdh * h, axis=(1, 2)),
                      (h.shape[1], h.shape[2], 1)).transpose()
        return (2 * theta_norm / ss) * (djdh - sss)

    def Jfid_dtheta_bl(self, g, f, h, theta_bl):
        djdh = self.Jfid_dh(g, f, h)
        Djdh = fft2D(djdh)
        djdblh = corr2D_K(Djdh, self.par.get("Bl"))
        return (2 * djdblh * theta_bl)

    def Jfid_dtheta_norm_bl(self, g, f, h, theta_norm_bl):
        djdh = self.Jfid_dh(g, f, h)
        Djdh = fft2D(djdh)
        djdblh = corr2D_K(Djdh, self.par.get("Bl"))
        ss = np.tile(np.sum(theta_norm_bl**2, axis=(1, 2)),
                     (h.shape[1], h.shape[2], 1)).transpose()
        sss = np.tile(np.sum(djdh * h, axis=(1, 2)),
                      (h.shape[1], h.shape[2], 1)).transpose()
        return (2 * theta_norm_bl / ss) * (djdblh - sss)

###########################################################################################
if __name__ == '__main__':
    '''
    test this class
    '''

    par = GlobalVar()
    img = Img(par)
    init = Initialization(par, img)

    imgData = img.read_image_data()

    g = init.init_g(imgData)
    f = init.init_f(par, imgData)
    h = init.init_h(par, imgData)

    fide = Fidelity(par, g)

    # check run time
    fide.updateCommon(g, f, h)

    res = fide.Jfid(g, f, h)
    res1 = np.mean(np.abs(g)**2)
    res2 = np.mean(np.abs(h)**2)

    df1_2d = fide.Jfid_df(g, f, h)
    df2_2d = fide.Jfid_dphi(g, f, h)
    df3_3d = fide.Jfid_dh(g, f, h)
    df4_3d = fide.Jfid_dtheta(g, f, h)
    # df5_3d = fide.Jfid_dtheta_norm(g, f, h)

    dat = np.zeros(shape=f.shape, dtype=float)

    dat = df1_2d
    plt.rcParams['font.size'] = 20
    plt.subplot(2, 2, 1)
    plt.imshow(dat, cmap='gray')
    plt.colorbar()
    plt.title('dJ/df')

    dat = df2_2d
    plt.subplot(2, 2, 2)
    plt.imshow(dat, cmap='gray')
    plt.colorbar()
    plt.title('dJ/dphi')

    dat[:, :] = df3_3d[0, :, :]
    plt.subplot(2, 2, 3)
    plt.imshow(dat, cmap='gray')
    plt.colorbar()
    plt.title('dJ/dh')

    # dat[:,:] = df5_3d[0,:,:]
    # plt.subplot(2, 2, 4)
    # plt.imshow(dat, cmap='gray')
    # plt.colorbar()
    # plt.title('dJ/dnormh')

    plt.show()

    print(g.shape)
