# -*- coding: utf-8 -*-
# config.py 用于读出,修改全局变量

import configparser
import os
import sys
from datetime import datetime

import numpy as np

from mpiAdapter import MPIAdapter


class GlobalVar:

    ''' 类变量作为全局变量，为该类所有实例共享 '''
    global_dict = {}

    def __init__(self, config):
        """ 将 config.in 读入并保存到字典 _global_dict """
        cfg = configparser.ConfigParser()
        cfg.read(config, encoding='utf-8')

        # MPI
        mpiadapter = MPIAdapter()
        GlobalVar.global_dict.update({'mpiadapter': MPIAdapter()})

        # [File]
        ImgFilePath = cfg['File']['ImgFilePath'].strip('", ')
        ImgFileFormat = cfg['File']['ImgFileFormat'].strip('", ')
        ResultFilePath = cfg['File']['ResultFilePath'].strip('", ') + '_' + ImgFileFormat.upper() + '_' + datetime.now().strftime("%Y_%m_%d-%H_%M_%S")
        if mpiadapter.isMaster():
            if os.path.exists(ResultFilePath):
                os.remove(ResultFilePath)
            else:
                os.mkdir(ResultFilePath)

        GlobalVar.global_dict.update({'ImgFilePath': ImgFilePath})
        GlobalVar.global_dict.update({'ImgFileFormat': ImgFileFormat})
        GlobalVar.global_dict.update({'ResultFilePath': ResultFilePath})

        # 确定指定格式的输入文件数量
        rootpath = '{0}{1}'.format(ImgFilePath, '/')
        abspath = os.path.abspath(rootpath)
        allfiles = os.listdir(abspath)
        nImg = 0
        imgList = []
        for f in allfiles:
            # 判断是不是文件
            absf = os.path.join(abspath, f)
            if os.path.splitext(absf)[-1].lower() == '.' + ImgFileFormat.lower():
                nImg = nImg + 1
                imgList.append(f)
        if nImg < 1:
            print('Can not find any file of format ".{}" in folder "{}/", exit now!'.format(ImgFileFormat, ImgFilePath))
            sys.exit()
        GlobalVar.global_dict.update({'nImg': str(nImg)})
        GlobalVar.global_dict["imgList"] = []
        for img in imgList:
            GlobalVar.global_dict["imgList"].append(img)

        # [Registration]
        GlobalVar.global_dict.update({'ImgRgdRegSel': cfg['Registration']['ImgRgdRegSel'].strip('", ')})
        GlobalVar.global_dict.update({'RotSmoothStat': cfg['Registration']['RotSmoothStat'].strip('", ')})
        GlobalVar.global_dict.update({'RotSmoothFit': cfg['Registration']['RotSmoothFit'].strip('", ')})

        # [Method]
        GlobalVar.global_dict.update({'BDfParaMethod': cfg['Method']['BDfParaMethod'].strip('", ')})
        GlobalVar.global_dict.update({'BDhParaMethod': cfg['Method']['BDhParaMethod'].strip('", ')})
        GlobalVar.global_dict.update({'BDInitFMethod': cfg['Method']['BDInitFMethod'].strip('", ')})
        GlobalVar.global_dict.update({'BDInitHMethod': cfg['Method']['BDInitHMethod'].strip('", ')})
        GlobalVar.global_dict.update({'BDMixnoiseStat': cfg['Method']['BDMixnoiseStat'].strip('", ')})

        # [Regularization]
        GlobalVar.global_dict.update({'BDfRegularMethod': cfg['Regularization']['BDfRegularMethod'].strip('", ')})
        GlobalVar.global_dict.update({'BDfRegularCoef': cfg['Regularization']['BDfRegularCoef'].strip('", ')})
        GlobalVar.global_dict.update({'BDfRegularTVIndex': cfg['Regularization']['BDfRegularTVIndex'].strip('", ')})

        # [Telescope]
        GlobalVar.global_dict.update({'ccdnread': cfg['Telescope']['ccdnread'].strip('", ')})
        GlobalVar.global_dict.update({'ccdu': cfg['Telescope']['ccdu'].strip('", ')})
        GlobalVar.global_dict.update({'ccdN': cfg['Telescope']['ccdN'].strip('", ')})
        GlobalVar.global_dict.update({'minlambda': cfg['Telescope']['minlambda'].strip('", ')})
        GlobalVar.global_dict.update({'maxlambda': cfg['Telescope']['maxlambda'].strip('", ')})
        GlobalVar.global_dict.update({'cenlambda': cfg['Telescope']['cenlambda'].strip('", ')})
        GlobalVar.global_dict.update({'D0': cfg['Telescope']['D0'].strip('", ')})
        GlobalVar.global_dict.update({'f0': cfg['Telescope']['f0'].strip('", ')})
        GlobalVar.global_dict.update({'r0': cfg['Telescope']['r0'].strip('", ')})

        # [Iteration]
        GlobalVar.global_dict.update({'BDMaxIter': cfg['Iteration']['BDMaxIter'].strip('", ')})
        FIter_nonorm_noreg = list(map(int, cfg.get('Iteration', 'FIter_nonorm_noreg').split(',')))
        FIter_nonorm_reg = list(map(int, cfg.get('Iteration', 'FIter_nonorm_reg').split(',')))
        FIter_norm_noreg = list(map(int, cfg.get('Iteration', 'FIter_norm_noreg').split(',')))
        FIter_norm_reg = list(map(int, cfg.get('Iteration', 'FIter_norm_reg').split(',')))
        FIter_BL_nonorm = list(map(int, cfg.get('Iteration', 'FIter_BL_nonorm').split(',')))
        FIter_BL_norm_noreg = list(map(int, cfg.get('Iteration', 'FIter_BL_norm_noreg').split(',')))
        FIter_BL_norm_reg = list(map(int, cfg.get('Iteration', 'FIter_BL_norm_reg').split(',')))

        HIter_nonorm_noreg = list(map(int, cfg.get('Iteration', 'HIter_nonorm_noreg').split(',')))
        HIter_nonorm_reg = list(map(int, cfg.get('Iteration', 'HIter_nonorm_reg').split(',')))
        HIter_norm_noreg = list(map(int, cfg.get('Iteration', 'HIter_norm_noreg').split(',')))
        HIter_norm_reg = list(map(int, cfg.get('Iteration', 'HIter_norm_reg').split(',')))
        HIter_BL_nonorm = list(map(int, cfg.get('Iteration', 'HIter_BL_nonorm').split(',')))
        HIter_BL_norm_noreg = list(map(int, cfg.get('Iteration', 'HIter_BL_norm_noreg').split(',')))
        HIter_BL_norm_reg = list(map(int, cfg.get('Iteration', 'HIter_BL_norm_reg').split(',')))

        niter = self.getInt('BDMaxIter')
        FIterNum = np.zeros(niter, dtype=int)
        HIterNum = np.zeros(niter, dtype=int)
        FIterNum[:] = 5
        HIterNum[:] = 5

        # 判断输入参数
        BDhParaMethod = self.getString("BDhParaMethod").lower()
        BDfRegularMethod = self.getString("BDfRegularMethod").lower()
        if BDhParaMethod == 'hnonnegative_normalization':  # normalization constrain
            if BDfRegularMethod == 'none':                 # no regularization
                FIterNum[:5] = FIter_norm_noreg[:5]
                FIterNum[5:] = FIter_norm_noreg[4]
                HIterNum[:5] = HIter_norm_noreg[:5]
                HIterNum[5:] = HIter_norm_noreg[4]
            else:
                FIterNum[:5] = FIter_norm_reg[:5]
                FIterNum[5:] = FIter_norm_reg[4]
                HIterNum[:5] = HIter_norm_reg[:5]
                HIterNum[5:] = HIter_norm_reg[4]
        elif BDhParaMethod == 'hnonnegative_bandlimited':
            FIterNum[:5] = FIter_BL_nonorm[:5]
            FIterNum[5:] = FIter_BL_nonorm[4]
            HIterNum[:5] = HIter_BL_nonorm[:5]
            HIterNum[5:] = HIter_BL_nonorm[4]  
        elif BDhParaMethod == 'hnonnegative_normalization_bandlimited':
            if BDfRegularMethod == 'none':                 # no regularization
                FIterNum[:5] = FIter_BL_norm_noreg[:5]
                FIterNum[5:] = FIter_BL_norm_noreg[4]
                HIterNum[:5] = HIter_BL_norm_noreg[:5]
                HIterNum[5:] = HIter_BL_norm_noreg[4]
            else:
                FIterNum[:5] = FIter_BL_norm_reg[:5]
                FIterNum[5:] = FIter_BL_norm_reg[4]
                HIterNum[:5] = HIter_BL_norm_reg[:5]
                HIterNum[5:] = HIter_BL_norm_reg[4]                     
        else:                                                         # regularization
            if BDfRegularMethod == 'none':
                FIterNum[:5] = FIter_nonorm_noreg[:5]
                FIterNum[5:] = FIter_nonorm_noreg[4]
                HIterNum[:5] = HIter_nonorm_noreg[:5]
                HIterNum[5:] = HIter_nonorm_noreg[4]
            else:
                FIterNum[:5] = FIter_nonorm_reg[:5]
                FIterNum[5:] = FIter_nonorm_reg[4]
                HIterNum[:5] = HIter_nonorm_reg[:5]
                HIterNum[5:] = HIter_nonorm_reg[4]

        # write back to global_dict
        str0 = ', '.join(str(i) for i in FIterNum)
        str1 = ', '.join(str(i) for i in HIterNum)

        self.set('FIterNum', str0)
        self.set('HIterNum', str1)

        self.FIterNum = FIterNum
        self.HIterNum = HIterNum

    def __call__(self, key):
        return self.global_dict[key]

    def set(self, key, value=None):
        """ 定义一个全局变量 """
        GlobalVar.global_dict.update({key: value})

    def get(self, key):
        try:
            return self.global_dict[key]
        except KeyError:
            return None

    def getString(self, key):
        """ 获得一个全局变量,不存在则返回默认值 """
        try:
            return self.global_dict[key]
        except KeyError:
            return None

    def getInt(self, key):
        """ 获得一个全局变量,不存在则返回 0 """
        try:
            return int(self.global_dict[key])
        except KeyError:
            return None

    def getFloat(self, key):
        """ 获得一个全局变量,不存在则返回 0 """
        try:
            return float(self.global_dict[key])
        except KeyError:
            return None

    def getIterNumFH(self):
        """
        根据不同的约束条件，从config.in中读取优化图像函数F所需的最小/最大迭代步数
        返回一个长度为 BDMaxIter list,但输入文件中只列出5项
        """
        # print(GlobalVar.global_dict)
        fnum = self.getString('FIterNum').split(',')
        hnum = self.getString('HIterNum').split(',')

        # convert to list of int
        numff = list(map(int, fnum))
        numhh = list(map(int, hnum))

        return numff, numhh

    def saveLog(self, filename=None):
        if not self('mpiadapter').isMaster():
            return

        rstFolder = self('ResultFilePath')
        if not os.path.exists(rstFolder):
            os.mkdir(rstFolder)

        f_id = open("{}/log.txt".format(rstFolder), 'w')
        f_id.write("{:30s} {:100s}\n".format('Parameter Name', 'Parameter Value'))
        # f_id.write("{:30s} {:100s}\n".format('ImgRgdRegSel', self.getString('ImgRgdRegSel')))
        # f_id.write("{:30s} {:100s}\n".format('RotSmoothStat', self.getString('RotSmoothStat')))
        # f_id.write("{:30s} {:100s}\n".format('RotSmoothFit', self.getString('RotSmoothFit')))
        f_id.write("{:30s} {:100s}\n".format('BDfParaMethod', self.getString('BDfParaMethod')))
        f_id.write("{:30s} {:100s}\n".format('BDhParaMethod', self.getString('BDhParaMethod')))
        f_id.write("{:30s} {:100s}\n".format('BDInitFMethod', self.getString('BDInitFMethod')))
        f_id.write("{:30s} {:100s}\n".format('BDInitHMethod', self.getString('BDInitHMethod')))
        f_id.write("{:30s} {:100s}\n".format('BDfRegularMethod', self.getString('BDfRegularMethod')))
        f_id.write("{:30s} {:100s}\n".format('BDfRegularFactor', self.getString('BDfRegularCoef')))
        f_id.write("{:30s} {:100s}\n".format('NumberOfImage', self.getString('nImg')))
        f_id.write("{:30s} {:100s}\n".format('BDMaxIter', self.getString('BDMaxIter')))
        f_id.write("{:30s} {:100s}\n".format('BDfRegularCoef', self.getString('BDfRegularCoef')))
        # f_id.write("{:30s} {:100s}\n".format('BDMixnoiseStat', self.getString('BDMixnoiseStat')))
        f_id.write("{:30s} {:100s}\n".format('Iterative F Step', self.getString('FIterNum')))
        f_id.write("{:30s} {:100s}\n".format('Iterative H Step', self.getString('HIterNum')))
        f_id.close()

    @staticmethod
    def PupilMask(fcutoff, sizeH):
        '''
        计算望远镜光瞳函数
        :param fcutoff: 望远镜截止频率
        :param sizeH: 望远镜光瞳函数大小（等于图像大小）
        :returns:
            P: 望远镜光瞳函数
            S: 光瞳函数面积    
        '''
        if len(sizeH) == 1:
            M = sizeH
            N = sizeH
        else:
            M = sizeH[-2]
            N = sizeH[-1]

        x = (np.arange(1, M + 1) - (M + 1) / 2) / (fcutoff / 2)
        y = (np.arange(1, N + 1) - (N + 1) / 2) / (fcutoff / 2)
        yy, xx = np.meshgrid(y, x)
        r = np.sqrt(xx**2 + yy**2)
        P = (r < 1).astype('float')
        P[r == 1] = 0.5
        S = P.sum()

        if len(sizeH) == 3:
            P = np.tile(P, (sizeH[0], 1, 1))
        return P, S


if __name__ == '__main__':
    '''
    test this py
    '''

    par = GlobalVar()

    Fnum, Hnum = par.getIterNumFH()

    print(Fnum, Hnum, sep='\n')

    par.set('Test', 'mytest')
    print(GlobalVar.global_dict)

    par.saveLog()
