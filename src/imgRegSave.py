import os
import re
import sys
from datetime import datetime

import cv2
import fitsio
import numpy as np

from globalvar import GlobalVar
from preprocess import RegImg
from Img import Img


class ImgRegSave:
    """
    配准并保存待复原图像，用于后续图像盲复原
    """

    def __init__(self, _par: GlobalVar, filenames=[]):
        self.par = _par

        # 读入输入参数，'ImgFilePath','ImgFileFormat'
        self.path = self.par.getString('ImgFilePath')
        self.format = self.par.getString('ImgFileFormat').lower()

        # 获得输入文件名列表，并进行排序
        allFilelist = self.get_filenames() if len(filenames) == 0 else filenames
        allFilelist.sort(key=self.digit_sort)
        self.filelist = allFilelist

        sampleImg = self.loadAImg(allFilelist[0])
        self.height = sampleImg.shape[0]
        self.width = sampleImg.shape[1]
        self.nImg = len(self.filelist)

    def reg_save(self):
        '''
        读入待处理图像，进行图像配准，将配准后图像写入指定文件夹，同时将盲解的输入文件夹更改为该文件夹
        '''
        imgdata = self.read_image_data()
        regimgdata = RegImg(self.par, imgdata)
        self.save_image_data(regimgdata)

    def get_filenames(self):
        '''
        :param path: 搜索文件的目录名，在该目录下找出所有后缀为 '.fits .bmp'的文件
        :return: 找到的文件列表
        '''
        rootpath = '{0}{1}'.format(self.path, '/')
        abspath = os.path.abspath(rootpath)
        allfiles = os.listdir(abspath)

        outfile = []
        filenames = []
        for file in allfiles:
            absfile = os.path.join(abspath, file)

            # 判断是不是文件
            if os.path.isfile(absfile):
                # 判断后缀是不是指定的格式
                ext = os.path.splitext(absfile)[-1]
                ext0 = '.' + self.format
                if ext == ext0:
                    outfile.append(absfile)
                    filenames.append(file)
        self.fileNames = filenames

        return outfile

    def loadAImg(self, imgPath):
        '''
        load A img from file
        '''
        if self.format == "bmp":
            return cv2.imread(imgPath, flags=cv2.IMREAD_GRAYSCALE)
        else:
            return fitsio.read(imgPath)

    def digit_sort(self, mystr):
        '''
        根据输入的字符串中部分(文件名basename)中的数字的升序进行排列
        :param my_list: 输入的字符串list
        :return: 输出排序完成后的字符串
        '''
        # 去掉目录
        s0 = os.path.split(mystr)[1]
        # 去掉后缀名
        s1 = os.path.splitext(s0)[0]
        # 分离尾部数字部分，如果没有，则为0
        s2 = re.findall(r'[^\D]+$', s1)

        s3 = []
        for s in s2:
            if s.isdigit():
                s3.append(int(s))
            else:
                s3.append(0)

        return s3

    def checkImgExist(self, imgPath):
        '''
        check if img is exist
        '''
        if not os.path.exists(imgPath):
            print('图像 ', imgPath, ' 不存在，程序将退出')
            sys.exit()

    def read_image_data(self):
        '''
        read image data from file list
        :param filelist:
        :return: image data from a batch of files
        '''

        imgdata = np.zeros([self.nImg, self.height, self.width])

        for ith in range(len(self.filelist)):
            curImg = self.filelist[ith]
            self.checkImgExist(curImg)
            data = self.loadAImg(curImg)
            # 判断每个图像文件是不是大小相同
            assert self.height == data.shape[0]
            assert self.width == data.shape[1]

            # 检查是否存在 < 0 的值
            imgdata[ith] = np.where(data < 0, 1e-14, data)

        return imgdata

    def save_image_data(self, imgdata):
        '''
        保存配准的待复原图像到指定文件夹，同时更改盲解的输入文件夹为该文件夹
        :param imgdata: 配准的待复原图像
        :return: 无
        '''
        dirs = '../' + self.path.split('/')[-1] + '_' + self.format.upper() + '_reg' + '_' + datetime.now().strftime("%Y_%m_%d-%H_%M_%S")
        self.par.set("ImgFilePath", dirs)
        if not os.path.exists(dirs):
            os.makedirs(dirs)
        for ith in range(self.nImg):
            fmtStr = self.par.getString("ImgFileFormat")
            path = "{}/{}".format(dirs, self.fileNames[ith])
            if fmtStr == "bmp":
                Img.save(imgdata[ith], path)
            elif fmtStr == "fits":
                fitsio.write(path, imgdata[ith])
