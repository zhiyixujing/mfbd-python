import sys

import matplotlib.pyplot as plt
import numpy as np

from BDtools import corr2D
from globalvar import GlobalVar
from Img import Img
from mpiAdapter import MPIAdapter


class Initialization:
    '''
    Initialization, 根据参数设置，初始化点扩散函数 h 和 图像函数 f
    '''

    def __init__(self, _par: GlobalVar, _img: Img):
        '''
        构造函数传入两个类参量_par, _img
        '''
        self.par = _par
        self.img = _img

    # def telscope_fstop(self):
    #     """望远镜参数,获得望远镜对应的截止频率"""
    #     ccdnread = self.par.getFloat('ccdnead')     # 探测器读出噪声
    #     ccdu = self.par.getFloat('ccdu')        # 探测器像元大小 u(um)
    #     ccdN = self.par.getFloat('ccdN')        # 探测器采样点 N
    #     f0 = self.par.getFloat('f0')           # 望远镜焦距 f(m)
    #     D0 = self.par.getFloat('D0')           # 望远镜口径 D(m)
    #     r0 = self.par.getFloat('r0')          # Fried常数 r0(m)
    #     minlam = self.par.getFloat('minlambda')   # 成像波段最小波长 lambda(um)
    #     maxlam = self.par.getFloat('maxlambda')   # 成像波段最大波长 lambda(um)
    #     cenlam = self.par.getFloat('cenlambda')   # 成像波段中心波长 lambda(um)

    #     fstop = np.round(ccdN * ccdu * D0 / minlam / f0)
    #     return fstop

    def init_input(self, g):
        '''
        根据不同的输入参数，初始化迭代输入
        :param:  g 为 3D 观察图像
        :return: myf, myh
        '''

        par = self.par
        fMethod = par.getString("BDfParaMethod").lower()
        if fMethod == 'none':
            myf = self.init_f(par, g)
        elif fMethod == 'fnonnegative':
            myf = self.init_phi(par, g)
        else:
            myf = None
            print('BDfParaMethod={} not defined in "GlobalVar"'.format(fMethod))
            sys.exit()

        hMethod = par.getString("BDhParaMethod").lower()
        if hMethod == 'none':
            myh = self.init_h(par, g)
        elif hMethod in ['hnonnegative', 'hnonnegative_normalization', 'hnonnegative_bandlimited', 'hnonnegative_normalization_bandlimited']:
            myh = self.init_theta(par, g)
        # elif par.BDhParaMethod.lower() == 'hnonnegative_normalization':
        #     myh = self.init_theta_norm(par, g)
        else:
            myh = None
            print('BDhParaMethod={} not defined in "GlobalVar"'.format(hMethod))
            sys.exit()

        return myf, myh

    def init_g(self, im):
        '''
        对观察图像g进行归一化，和初始化
        :param im: 观察图像 序列 [nfiles, height,width]
        :return: 正规化后的g 以及FFT变换后的 G（G.dtype=complex）
        '''
        # 使序列图像中每一幅图像的像素和相同
        g = np.abs(im)
        g0 = MPIAdapter.avage(np.mean(im))
        g1 = np.mean(im, axis=(1, 2))
        for ith in range(0, g.shape[0]):
            g[ith, :, :] = g[ith, :, :] / g1[ith] * g0
        return g

    def init_f(self, par, im):
        '''
        根据输入控制参数，对实际图像f进行初始化
        :param im: 观察到真实图像 [height,width], 需要注意的是 即使是多帧图像ith>1，真实图像仍然只是 1帧, 因此 f 总是 2D的
        :param par: 输入控制类
        :return: 初始化后的真实图像 f, 2D, dtype=float
        '''

        im = np.abs(im)
        f = np.zeros(shape=(im.shape[1], im.shape[2]), dtype=float)
        method = self.par.getString("BDInitFMethod").lower()
        if method == 'fMean'.lower():
            f = MPIAdapter.avage(np.mean(im, axis=0))  # 对所有帧进行算术平均值
        elif method == 'fMed'.lower():
            f = np.median(im, axis=0)     # 对所有帧图像像素排序后的中位值
        else:
            print('Parameter BDInitFMethod="{}" unsupported!'.format(method))
            sys.exit()
        return f

    def init_phi(self, par, im):
        '''
        :param im: 观察到真实图像 [height,width], 需要注意的是 即使是多帧图像ith>1，真实图像仍然只是 1帧, 因此 f 总是 2D的
        :param par: 输入控制类
        :return: 初始化后的真实图像 phi, phi = sqrt(f), dtype=float
        '''

        return np.sqrt(self.init_f(par, im))

    def init_h(self, par, im):
        '''
        根据输入控制参数，对点扩散函数h进行初始化
        :param im: 观察到真实图像 [nfiles，height,width]
        :param par: 输入控制类
        :return: 初始化后的点扩散函数 h, 3D, dtype=float
        '''

        im = np.abs(im)
        method = self.par.getString("BDInitHMethod").lower()
        if method == 'hSame'.lower():      # 等值矩阵
            h = np.ones_like(im)
            h[:, :, :] = h[:, :, :] / (h.shape[1] * h.shape[2])

        elif method == 'hConv'.lower():    # Biggs初值方法
            hpara = 0.01
            h = np.zeros(shape=im.shape, dtype=float)
            # 相关运算 g @ g
            Rgg = np.abs(corr2D(im, im))
            for ith in range(0, im.shape[0]):
                h[ith, :, :] = (Rgg[ith, :, :] - np.min(Rgg[ith, :, :]) +
                                hpara * (np.max(Rgg[ith, :, :]) - np.min(Rgg[ith, :, :])))
                h[ith, :, :] /= np.sum(h[ith, :, :])
        else:
            print('Parameter BDInitHMethod="{}" not defined!'.format(method))
            sys.exit()

        for ith in range(h.shape[0]):
            h[ith, :, :] = h[ith, :, :] / np.sum(h[ith, :, :])

        return h

    def init_theta(self, par, im):
        '''
        根据输入控制参数，对点扩散函数h进行初始化
        :param im: 观察到真实图像 [nfiles，height,width]
        :param par: 输入控制类
        :return: 初始化后的点扩散函数 theta= sqrt(h), 3D, dtype=float
        '''

        return np.sqrt(self.init_h(par, im))

    # def init_theta_norm(self, par, im):
    #     '''
    #     根据输入控制参数，对点扩散函数h进行初始化
    #     :param im: 观察到真实图像 [nfiles，height,width]
    #     :param par: 输入控制类
    #     :return: 初始化后的点扩散函数 h= theta*theta /sum(theta*theta), 3D, dtype=float
    #     '''

    #     im = np.abs(im)
    #     norm = np.zeros_like(im)
    #     h0 = self.init_h(par, im)
    #     eps = np.finfo(np.float32).eps
    #     sumh = np.sum(h0, axis=(1, 2)) + eps   # 防止被 0 除
    #     for ith in range(0, im.shape[0]):
    #         norm[ith, :, :] = np.sqrt(h0[ith, :, :] / sumh[ith])
    #     return norm


if __name__ == '__main__':
    '''
    test this py file
    '''
    par = GlobalVar()
    img = Img(par)

    init = Initialization(par, img)

    imgData = img.read_image_data()
    print('fstop=', init.telscope_fstop())

    d = np.zeros(shape=(img.height, img.width))
    g = init.init_g(imgData)

    f = init.init_f(par, imgData)

    h = init.init_h(par, imgData)

    phi = init.init_phi(par, imgData)

    theta = init.init_theta(par, imgData)

    thetanorm = init.init_theta_norm(par, imgData)

    myf, myh = init.init_input(g)

    d[:, :] = np.abs(g[0, :, :])
    plt.rcParams['font.size'] = 20
    plt.subplot(1, 3, 1)
    plt.imshow(d, interpolation='nearest', cmap='bone')
    plt.colorbar()

    d[:, :] = np.abs(theta[0, :, :])
    plt.subplot(1, 3, 2)
    plt.imshow(d, interpolation='nearest', cmap='bone')
    plt.colorbar()

    d[:, :] = np.abs(thetanorm[0, :, :])
    plt.subplot(1, 3, 3)
    plt.imshow(d, interpolation='nearest', cmap='bone')
    plt.colorbar()

    plt.show()
