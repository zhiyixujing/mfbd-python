import matplotlib.pyplot as plt
import numpy as np

from mpiAdapter import MPIAdapter


def braket(f, x0, dx0, x1=None, x2=None, a=None, b=None, tol=1.0e-6, maxiter=20):
    """
    这是所有1D 线搜索的基础，通过设定的曲线上两个点，xa,xb， 以及其函数值 fa,fb,
    通过三点抛物线插值方法寻找第三个点xc, 使得 xc介于 xa, xb之间，并且 fc<fa, fc<fb,
    这样能保证local minima 位于xa, xc之间。
    parameters:
    input f:         优化函数
    input x0:         nD float np.array, 初始位置， f的第一个输入参数
    input dx0:        nD float np.array， 搜索方向
    input x1:         nD float np.narray, f的第二个输入参数
    input x2:         nD float np.narray, f的第三个输入参数
    input a,b         float, 初始猜测值，optional
    input tol:        float, 容差
    input maxiter:    最大迭代步数

    output xa, xb, xc:  包含local minima的三元组，满足 xa<xb<xc 或 xa>xb>xc, 但是 fa>fb<fc
    """
    # 定义常数
    GOLD = 1.618034
    GLIMIT = 100
    EPS = np.finfo(float).eps

    # 归一化 dx0
    # dx0 = dx0/np.sqrt(np.sum(dx0*dx0))

    # 先猜测2个点，a,b, 表示沿着 dx0 的
    if a is None:
        if len(np.unique(x0)) == 1:  # 等值矩阵
            std_x0 = 0.
        else:
            std_x0 = np.std(x0, ddof=1)
        a = std_x0 / np.std(dx0, ddof=1) * 0.01
    a = MPIAdapter.avage(a)
    if b is None:
        b = 1.618 * a + 0.001 * (a == 0)
    
    fa = f(x0 + a * dx0, x1, x2)
    fb = f(x0 + b * dx0, x1, x2)

    # 确保 fb<fa
    if (fb > fa):
        temp = a
        a = b
        b = temp
        temp = fa
        fa = fb
        fb = temp

    # 确定一个点 c
    c = b + GOLD * (b - a)
    fc = f(x0 + c * dx0, x1, x2)

    niter = 0
    while (fb > fc):
        niter = niter + 1
        r = (b - a) * (fb - fc)
        q = (b - c) * (fb - fa)
        deno = 2 * np.max([np.abs(q - r), EPS])
        u = b - ((b - c) * q - (b - a) * r) / SIGN(deno, q - r)

        ulim = b + GLIMIT * (c - b)
        if (b - u) * (u - c) > 0:                       # u 在 b, c 之间
            fu = f(x0 + u * dx0, x1, x2)
            if (fu < fc):
                a = b
                b = u
                fa = fb
                fb = fu
                break
            elif (fu > fb):
                c = u
                fc = fu
                break
            u = c + GOLD * (c - b)
            fu = f(x0 + u * dx0, x1, x2)

        elif (c - u) * (u - ulim) > 0:  # u 在 c, ulim 之间
            fu = f(x0 + u * dx0, x1, x2)
            if (fu < fc):
                b = c
                c = u
                u = u + GOLD * (u - b)  # 原程序此处有误 c-->b
                fb = fc
                fc = fu
                fu = f(x0 + u * dx0, x1, x2)

        elif (u - ulim) * (ulim - c) > 0:  # ulim 在 u, c 之间， 采用 ulim
            u = ulim
            fu = f(x0 + u * dx0, x1, x2)

        else:  # 不采用 抛物线最小值点 u
            u = c + GOLD * (c - b)
            fu = f(x0 + u * dx0, x1, x2)

        a = b
        b = c
        c = u
        fa = fb
        fb = fc
        fc = fu

    # 至此得到a,b,c和f(a),f(b),f(c)
    if b <= np.min([a, c]) or b >= np.max([a, c]):
        print('bracket() error：没有找到极小值区间!')
    if niter >= maxiter:
        print('bracket() warning：达到最大迭代步数，可能不收敛!')

    # xa= x0 + dx0 * a;   xb= x0 + dx0 * b;   xc= x0 + dx0 * c
    # print(xa, xb, xc, f(xa), f(xb), f(xc))
    return a, b, c, fa, fb, fc


#######################################
def brent(f, x0, dx0, x1=None, x2=None, tol=1.0e-5, maxiter=50):
    """
        采用‘不用’导数的 Brent算法， 沿着一个特定方向 dx0, 搜索函数 f 的局部极值，得到极值对应的位置 xmin，和极值 fmin。
        Brent 搜索算法

        parameters:
        input f:         优化函数
        input x0:         nD float np.array, 初始位置， f的第一个输入参数
        input dx0:        nD float np.array， 搜索方向
        input x1:         nD float np.narray, f的第二个输入参数
        input x2:         nD float np.narray, f的第三个输入参数
        input tol:        float, 容差
        input maxiter:    最大迭代步数

        output xmin:      np.array, 最小值所在位置相对于x0 移动的距离， 即 x = x0 + xmin*dx0
        output fmin:      xmin对应的 f 的值
        """

    # 常数
    EPS = np.finfo(float).eps
    CGOLD = 0.3819660

    # 调用 braket() 确定包含 local minima的三元组 ax,bx,cx
    # dx0 = dx0 / np.sqrt(np.sum(dx0 * dx0))
    aa, bb, cc, __, __, __ = braket(f, x0, dx0, x1, x2, tol=tol, maxiter=maxiter)

    # 将aa, bb, cc 以升序排列, 然后赋予 a,b, 使得 a<b
    a = np.min([aa, bb, cc])
    b = np.max([aa, bb, cc])
    MPIAdapter.barrier()
    a = MPIAdapter.min(a)
    b = MPIAdapter.max(b)
    
    x = w = v = bb
    fw = fv = fx = f(x0 + x * dx0, x1, x2)
    d = np.array(0)
    e = np.array(0)

    niter = 0
    while (niter < maxiter):
        niter = niter + 1
        xm = 0.5 * (a + b)
        tol1 = tol * np.abs(x) + EPS
        tol2 = 2.0 * tol1

        if (niter >= maxiter):
            print('brent() warning：达到最大迭代步数，可能不收敛!')
            return xm, fx

        if (np.abs(x - xm) <= tol2 - 0.5 * (b - a)):     # 测试是否满足精度
            return xm, fx

        if (np.abs(e) > tol1):   # parabolic fit.
            r = (x - w) * (fx - fv)
            q = (x - v) * (fx - fw)
            p = (x - v) * q - (x - w) * r
            q = 2.0 * (q - r)

            p = -p if q > 0.0 else p
            q = np.abs(q)
            etemp = e
            e = d

            if (np.abs(p) >= np.abs(0.5 * q * etemp) or p <= q * (a - x) or p >= q * (b - x)):
                e = a - x if x >= xm else b - x
                d = CGOLD * e
            else:
                d = p / q
                u = x + d
                if (u - a < tol2 or b - u < tol2):
                    d = SIGN(tol1, xm - x)
        else:
            e = a - x if x >= xm else b - x
            d = CGOLD * e

        u = x + d if np.abs(d) >= tol1 else x + SIGN(tol1, d)
        fu = f(x0 + u * dx0, x1, x2)

        if (fu <= fx):
            if u >= x:
                a = x
            else:
                b = x
            v = w
            w = x
            x = u
            fv = fw
            fw = fx
            fx = fu

        else:
            if u < x:
                a = u
            else:
                b = u
            if (fu <= fw or w == x):
                v = w
                w = u
                fv = fw
                fw = fu
            elif (fu <= fv or v == x or v == w):
                v = u
                fv = fu
            else:
                pass


#######################################
def DBrent(f, fprime, x0, dx0, x1=None, x2=None, tol=1.0e-5, maxiter=50):
    '''
    % 计算过程需要求解a的导数
    '''

    # 常数
    EPS = np.finfo(float).eps
    # ZEPS = EPS
    ZEPS = EPS * 1.0e-3

    # 调用 braket() 确定包含 local minima的三元组 ax,bx,cx
    # dx0 = dx0 / np.sqrt(np.sum(dx0 * dx0))
    a, b, c, __, fb, __ = braket(f, x0, dx0, x1, x2, tol=tol, maxiter=maxiter)

    tol = 1e-8
    e = 0
    d = 0
    x = b
    w = b
    v = b
    db = np.mean(dx0 * fprime(x0 + b * dx0, x1, x2))
    fx = fw = fv = fb
    dx = dw = dv = db

    b = np.max([a, c])
    a = np.min([a, c])

    for i in range(maxiter):
        xm = 0.5 * (a + b)
        tol1 = tol * abs(x) + ZEPS
        tol2 = 2 * tol1
        if abs(x - xm) <= tol2 - 0.5 * (b - a):
            xmin = x
            fmin = fx
            break

        if abs(e) > tol1:
            d1 = 2 * (b - a)
            d2 = d1
            if (dw != dx):
                d1 = (w - x) * dx / (dx - dw)

            if (dv != dx):
                d2 = (v - x) * dx / (dx - dv)

            u1 = x + d1
            u2 = x + d2
            ok1 = ((a - u1) * (u1 - b) > 0) and (dx * d1 <= 0)
            ok2 = ((a - u2) * (u2 - b) > 0) and (dx * d2 <= 0)
            olde = e
            e = d
            if(ok1 or ok2):
                if(ok1 and ok2):
                    if abs(d1) < abs(d2):
                        d = d1
                    else:
                        d = d2
                elif ok1:
                    d = d1
                else:
                    d = d2

                if (abs(d) <= abs(0.5 * olde)):
                    u = x + d
                    if (u - a < tol2) or (b - u < tol2):
                        d = SIGN(abs(tol1), xm - x)
                else:
                    if dx >= 0:
                        e = a - x
                    else:
                        e = b - x
                    d = 0.5 * e
            else:
                if dx >= 0:
                    e = a - x
                else:
                    e = b - x
                d = 0.5 * e
        else:
            if dx >= 0:
                e = a - x
            else:
                e = b - x
            d = 0.5 * e

        if abs(d) >= tol1:
            u = x + d
            fu = f(x0 + u * dx0, x1, x2)
        else:
            if(d >= 0):
                u = x + abs(tol1)
            else:
                u = x - abs(tol1)
            fu = f(x0 + u * dx0, x1, x2)
            if fu > fx:
                fmin = fx
                xmin = x
                break

        du = np.mean(dx0 * fprime(x0 + u * dx0, x1, x2))
        if fu <= fx:
            if (u >= x):
                a = x
            else:
                b = x
            v = w
            fv = fw
            dv = dw
            w = x
            fw = fx
            dw = dx
            x = u
            fx = fu
            dx = du
        else:
            if u < x:
                a = u
            else:
                b = u
            if (fu <= fw) or (w == x):
                v = w
                fv = fw
                dv = dw
                w = u
                fw = fu
                dw = du
            elif (fu < fv) or (v == x) or (v == w):
                v = u
                fv = fu
                dv = du

    if(i == maxiter):
        print('DBracket() warning：达到最大迭代步数，可能不收敛!')

    return xmin, fmin


def SIGN(a, b):
    if b >= 0:
        return a
    else:
        return -a


###########################################
if __name__ == '__main__':

    # 测试 1D 函数
    def f(x, x1=None, x2=None):
        return (x + 3) * (x - 1)**2

    # 测试 2D 函数
    def ff(x, x1=None, x2=None):
        return x[0]**2 + 3 * x[1]**2

    plt.rcParams['font.size'] = 24
    x0 = np.linspace(-3, 3, 100)
    y0 = f(x0)
    # plt.plot(x0,y0)

    '''
    a,b,c,fa,fb,fc= braket(f, -100, 1)
    print('1D test for braket():', a,b,c,fa,fb,fc, '\n')
    
    xmin, fmin = brent(f, 1, 1)
    print('1D test for brent(), find ', xmin, fmin)
    '''

    nx = 100
    ny = 120
    x1 = np.linspace(-4, 4, nx)
    x2 = np.linspace(-3, 3, ny)
    out = np.zeros(shape=(nx, ny))
    for i in range(0, nx):
        for j in range(0, ny):
            xx = (x1[i], x2[j])
            out[i, j] = ff(xx)
    plt.pcolor(x2, x1, out)
    plt.colorbar()

    x0 = np.array([-3, -2])
    dx0 = np.array([1, 1])
    xmin, fmin = brent(ff, x0, dx0)
    x = x0 + xmin * dx0
    plt.scatter(x0[0], x0[1], color='w')
    plt.scatter(x[0], x[1], color='r')
    print('2D test for brent():', xmin, fmin)

    plt.show()
    quit()
