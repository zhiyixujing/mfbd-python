# -*- coding: UTF-8 -*-

import getopt
import os
import subprocess
import sys
import threading
import time
from queue import Queue

from mpi4py import MPI
from watchdog.events import FileSystemEventHandler
from watchdog.observers import Observer

from mpiAdapter import MPIAdapter

adapter = MPIAdapter()
filenames = {}
tasks = Queue()


class HandleNewImgs(FileSystemEventHandler):
    def __init__(self):
        FileSystemEventHandler.__init__(self)

    def findNewImgs(self):
        return len(filenames) > 0

    def on_moved(self, event):
        if event.is_directory:
            pass
            # print("directory moved from {0} to {1}".format(event.src_path, event.dest_path))
        else:
            pass
            # print("file moved from {0} to {1}".format(event.src_path, event.dest_path))

    def on_created(self, event):
        if event.is_directory:
            pass
        else:
            id = len(filenames)
            filenames[str(id)] = os.path.abspath(event.src_path)

    def on_deleted(self, event):
        if event.is_directory:
            pass
            # print("directory deleted:{0}".format(event.src_path))
        else:
            pass
            # print("file deleted:{0}".format(event.src_path))

    def on_modified(self, event):
        if event.is_directory:
            pass
            # print("directory modified:{0}".format(event.src_path))
        else:
            pass
            # print("file modified:{0}".format(event.src_path))


class WatchDirectory:
    '''
    监视特定目录，用于获取新的图像
    '''

    def __init__(self):
        self.observer_ = Observer()

    def monitor(self, path, handler):
        self.observer_.schedule(handler, path, True)
        self.observer_.start()
        try:
            while True:
                time.sleep(1)
        except KeyboardInterrupt:
            self.observer_.stop()
        self.observer_.join()


def updateWorkder():
    while True:
        time.sleep(1)
        workerId = adapter.recv(MPI.ANY_SOURCE)
        adapter.newWorker(workerId)


def pack(process_num, imgNameList):
    tmp = ""
    tmp = tmp.join(["%s;" % (v) for k, v in imgNameList.items()])
    return "mpirun -n {} python {}/mfbd.py -i \"{}\"".format(process_num, os.path.abspath('.'), tmp.strip(';'))


if __name__ == '__main__':
    '''
    entry point
    '''

    try:
        opts, args = getopt.getopt(sys.argv[1:], "hi:", ["imgsDirPath="])
    except getopt.GetoptError:
        print('mpirun -n x main.py -i <imgsDirPath>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('mpirun -n x main.py -i <imgsDirPath>')
            sys.exit()
        elif opt in ("-i", "--imgsDirPath"):
            imgDirPath = arg

    if adapter.isMaster():
        monitorThread = threading.Thread(target=WatchDirectory().monitor, args=(imgDirPath, HandleNewImgs()))
        monitorThread.start()
        updateWorkerThread = threading.Thread(target=updateWorkder)
        updateWorkerThread.start()

        while True:
            time.sleep(1)
            if len(filenames) > 0:
                tasks.put(filenames)
                filenames = {}

            if tasks.qsize() > 0 and adapter.hasIdleWorker():
                worker = adapter.getWorker()
                task = tasks.get()
                print('worker {} handle task {}'.format(worker, task))
                sys.stdout.flush()
                adapter.send(worker, task)
    else:
        while True:
            time.sleep(1)
            # state = subprocess.run(pack(2, adapter.recv(0)), shell=True, capture_output=True, encoding='utf8', text=True)
            process = subprocess.Popen(pack(2, adapter.recv(0)), stdout=subprocess.PIPE)
            for line in iter(process.stdout.readline, ""):
                sys.stdout.write(line)
                sys.stdout.flush()

            adapter.isend(0, adapter.rank())
