# -*- coding: UTF-8 -*-

import getopt
import sys

from alter_minimize import AlterMinimize
from fidelity import Fidelity
from globalvar import GlobalVar
from Img import Img
from imgRegSave import ImgRegSave
from initialization import Initialization
from objfunction import ObjFun
from regularization import Regularization


class MFBD():
    '''
    MFBD(Multi-Frame Blind Deconvolution),多帧图像盲复原
    '''

    def __init__(self, configFile):
        self.par = GlobalVar(configFile)
        self.mpiadapter = self.par('mpiadapter')
        self.rank = self.mpiadapter.rank()
        
        # 预处理-图像配准
        if self.par.getInt('ImgRgdRegSel') != 0:
            regImgFilePath = ''
            if self.rank == 0:
                print('进程 {}: 正在进行预处理-图像配准'.format(self.rank))
                sys.stdout.flush()
                regHandler = ImgRegSave(self.par)
                regHandler.reg_save()
                regImgFilePath = self.par.getString('ImgFilePath')
                print('进程 {}: 预处理-图像配准，完成'.format(self.rank))
                sys.stdout.flush()
            self.mpiadapter.barrier()
            regImgFilePath = self.mpiadapter.bcast(regImgFilePath, 0)
            self.par.set(key='ImgFilePath', value=regImgFilePath)

        
    def restore(self, imgFilenames=[]):
        # 图像输入输出类
        img = Img(self.par, imgFilenames)
        imgData = img.read_image_data()
            
        # 初始化图像
        init = Initialization(self.par, img)
        g = init.init_g(imgData)
        print('进程 {}: 读入观察图像序列，完成'.format(self.rank))
        sys.stdout.flush()
                    
        # 代价函数保真项及其导数类
        fide = Fidelity(self.par, g)

        # 正则化及其导数类
        regu = Regularization(self.par)

        # 收集代价函数类
        obj = ObjFun(self.par, regu, fide)

        # 交替迭代最小化代价函数的类
        alter = AlterMinimize(self.par, img, init, obj)

        # 输入参数保存到文件
        self.par.saveLog()
        
        # 初始化 myf myh
        # myf，myh是f和h的参量化表示，不是图像与PSF估计本身
        myf, myh = init.init_input(g)
            
        print('进程 {}: 初始化真实图像，完成'.format(self.rank))
        print('进程 {}: 初始化 PSF 图像，完成'.format(self.rank))
        print('进程 {}: 开始交替迭代，共 {} 步\n'.format(self.rank, self.par.getString("BDMaxIter")))
        sys.stdout.flush()
        # 采用交替最小化方法进行盲解卷积
        alter.alterMin(g, myf, myh)

        return True


if __name__ == '__main__':
    '''
    test this py file
    '''
    configFile = './config.in'
    inputfile = None
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hc:i:", ["config=", "ifile="])
    except getopt.GetoptError:
        print('python mfbd.py -i <inputfile> -c <configfile>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('python mfbd.py -i <inputfile> -c <configfile>')
            sys.exit()
        elif opt in ("-c", "--config"):
            configFile = arg.strip(' ')
        elif opt in ("-i", "--ifile"):
            inputfile = arg.strip(' ')

    mfbd = MFBD(configFile)
    if inputfile is None:
        inputfile = [mfbd.par.getString("ImgFilePath") + "/" + img for img in mfbd.par.get("imgList")]
        mfbd.restore(inputfile)
    else:
        mfbd.restore([item.strip(' ') for item in inputfile.split(';')])
