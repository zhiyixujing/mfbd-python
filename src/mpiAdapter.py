from queue import Queue

import numpy as np
from mpi4py import MPI


class MPIAdapter:
    '''
    MPI 包装类
    '''

    comm_ = None
    rank_ = None
    size_ = None

    def __init__(self):
        MPIAdapter.comm_ = MPI.COMM_WORLD
        MPIAdapter.rank_ = MPIAdapter.comm_.Get_rank()
        MPIAdapter.size_ = MPIAdapter.comm_.Get_size()
        self.pool_ = Queue()
        if self.isMaster():
            for i in range(1, MPIAdapter.size_):
                self.pool_.put(i, block=True)

    def isMaster(self):
        return MPIAdapter.rank_ == 0

    def rank(self):
        return MPIAdapter.rank_

    def size(self):
        return MPIAdapter.size_

    def send(self, id, val):
        MPIAdapter.comm_.send(val, dest=id)

    def isend(self, id, val):
        MPIAdapter.comm_.isend(val, dest=id)

    def recv(self, id):
        return MPIAdapter.comm_.recv(source=id)

    def irecv(self, id):
        return MPIAdapter.comm_.irecv(source=id)

    def hasIdleWorker(self):
        return self.pool_.qsize() > 0

    def getWorker(self):
        '''
        返回一个空闲的进程号
        '''
        if self.isMaster():
            if (not self.pool_.empty()):
                return self.pool_.get()
            else:
                return - 1
        else:
            pass

    def newWorker(self, n):
        '''
        向队列中加入一个空闲的进程号
        '''
        self.pool_.put(n, block=True)

    def distribute(self, n: int):
        rst = []

        step = max(1, round(n / MPIAdapter.size_))
        start = MPIAdapter.rank_ * step
        end = n if (MPIAdapter.rank_ == MPIAdapter.size_ - 1) else start + step

        for i in range(start, end):
            rst.append(i)

        return rst

    @staticmethod
    def all_gather(val):
        return MPIAdapter.comm_.allgather(val)

    @staticmethod
    def max(val):
        return MPIAdapter.comm_.allreduce(val, MPI.MAX)

    @staticmethod
    def min(val):
        return MPIAdapter.comm_.allreduce(val, MPI.MIN)

    @staticmethod
    def sum(val):
        return MPIAdapter.comm_.allreduce(val, MPI.SUM)

    @staticmethod
    def avage(val):
        return MPIAdapter.comm_.allreduce(val, MPI.SUM) / float(MPIAdapter.comm_.size)

    @staticmethod
    def barrier():
        MPIAdapter.comm_.Barrier()

    @staticmethod
    def bcast(val, root):
        return MPIAdapter.comm_.bcast(val, root)

    def id_range_at_global(self, num):
        ImgDis = MPIAdapter.all_gather(num)
        accumulateId = [x - x for x in range(self.size() + 1)]
        for i in range(1, self.size() + 1):
            accumulateId[i] = accumulateId[i - 1] + ImgDis[i - 1]

        return accumulateId


if __name__ == '__main__':
    '''
    test this py file
    '''

    adapter = MPIAdapter()
    print(MPIAdapter.avage(adapter.rank()))
    print(MPIAdapter.min(adapter.rank()))
    print(MPIAdapter.max(adapter.rank()))
    print(adapter.distribute(10))
