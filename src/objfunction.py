import datetime
import sys

import matplotlib.pyplot as plt
import numpy as np

from BDtools import fft2D, ifft2D
from fidelity import Fidelity
from globalvar import GlobalVar
from Img import Img
from initialization import Initialization
from mpiAdapter import MPIAdapter
from regularization import Regularization


class ObjFun:
    """
    处理最小化的目标函数以及其导数的类
    """

    def __init__(self, _par, _regular, _fide):
        '''

        :param _par:          # 控制输入参数的类
        :param _regular:      # 正则项、正则项导数相关类
        :param _fide          # 保证项、保证项导数相关类
        '''

        self.par = _par
        self.regular = _regular
        self.fidelity = _fide
        self.isRealImage = 0
        self.fmethod = self.par.getString("BDfParaMethod").lower()
        self.hmethod = self.par.getString("BDhParaMethod").lower()

    '''
    如下 costJ, costJdf, costJdh
    分别为 计算代价函数，代价函数对f导数，代价函数对h导数，其输入参数意义分别为:
    :param: g 观察图像，3D
    :param: myf, 迭代过程中估计的真实图像，2D
            myf= phi= sqrt(f),  :当 par.BDfParaMethod = "fNonnegative"，
            myf= f,             :当 par.BDfParaMethod = "None"

    :param: myh, 迭代过程中估计的 PSF图像，3D
            myh= theta= sqrt(h), :当 par.BDfParaMethod = "hNonnegative"，
            myh= theta_norm      :当 par.BDfParaMethod = 'hNonnegative_Normalization'
            myh= h,              :当 par.BDfParaMethod = "None"

            当调用 Fidelity 和 Regularization 中的一些方法时，需要将 phi*phi -->f, theta*theta -->h   
    '''

    def get_fh(self, myf, myh, getfh_flag=0):
        '''
        通过输入参数 par，将 myf,myh 转化成 f, h
        :param myf:
        :param myh:
        :return: f, h
        '''
        
        # f <-- myf
        if self.fmethod == 'none':
            f = np.abs(myf)
        elif self.fmethod == 'fnonnegative':
            f = np.abs(myf) * np.abs(myf)
        else:
            f = None
            print('BDfParaMethod={} not defined in "GlobalVar"'.format(self.fmethod))
            sys.exit()

        # h <-- myh
        if getfh_flag:
            h = np.abs(myh) * np.abs(myh)
        else:
            if self.hmethod == 'none':
                h = np.abs(myh)
            elif self.hmethod == 'hnonnegative':
                h = np.abs(myh) * np.abs(myh)
            elif self.hmethod == 'hnonnegative_normalization':
                h = myh * myh
                for ith in range(h.shape[0]):
                    h[ith] = h[ith] / np.sum(h[ith])
            elif self.hmethod == 'hnonnegative_bandlimited':
                sita = myh * myh
                H = fft2D(sita)*self.par.get("Bl")
                h = ifft2D(H)
            elif self.hmethod == 'hnonnegative_normalization_bandlimited':
                sita = myh * myh
                for ith in range(sita.shape[0]):
                    sita[ith] = sita[ith] / np.sum(sita[ith])
                H = fft2D(sita)*self.par.get("Bl")
                h = ifft2D(H)       
            else:
                h = None
                print('BDhParaMethod={} not defined in "GlobalVar"'.format(self.hmethod))
                sys.exit()

        return f, h

    def Jall(self, g, myf, myh, getfh_flag=0):
        '''
        代价函数， 包括两部分， ‘似然’+‘正则化’
        :param:  g, myf, myh 意义见上述说明
        :return: 正则项对应的代价函数，标量
        '''

        # 求出 f, h
        f, h = self.get_fh(myf, myh, getfh_flag)

        # 正则项对应的 cost
        J_reg = self.regular.Jreg(f)

        # 似然项对应的 cost
        J_fid = self.fidelity.Jfid(g, f, h)

        return (J_reg + J_fid)

    def Jall_df(self, g, myf, myh, getfh_flag=0):
        '''
        代价函数J对 myf的导数， 包括两部分， ‘似然’+‘正则化’
        :param:  g, myf, myh 意义见上述说明
        :return: 2D，dJ/df
        '''

        # 求出 f, h
        f, h = self.get_fh(myf, myh, getfh_flag)

        # dJreg/df, dJfid/df
        df_reg = np.zeros_like(myf)
        df_fid = np.zeros_like(myf)
        if self.fmethod == 'none':
            df_reg = self.regular.Jreg_df(f)
            df_fid = self.fidelity.Jfid_df(g, f, h)
        elif self.fmethod == 'fnonnegative':
            df_reg = self.regular.Jreg_dphi(f, myf)
            df_fid = self.fidelity.Jfid_dphi(g, f, h, myf)
        else:
            print('BDfParaMethod={} not defined in "GlobalVar"'.format(self.fmethod))
            sys.exit()

        return (df_reg + df_fid)

    def Jall_dh(self, g, myf, myh, getfh_flag=0):
        '''
        根据不同输入参数，返回 d{J_fid}/dh, d{J_fid}/d{theta}, d{J_fid}/d{theta_norm}
        正则项仅仅与真实图像 f有关，即 d{J_reg}/dh = 0
        :param g:    观察图像,3D
        :param myf:  真实图像，2D
        :param myh:  PSF估计，3D
        :return: d{J_fid}/dh or d{J_fid}/d{theta} or d{J_fid}/d{theta_norm}, 3D
        '''

        # 求出 f, h
        f, h = self.get_fh(myf, myh, getfh_flag)

        # dJreg/df, dJfid/df
        dh_fid = np.zeros_like(myh)

        if self.hmethod == 'none':
            dh_fid = self.fidelity.Jfid_dh(g, f, h)
        elif self.hmethod == 'hnonnegative':
            dh_fid = self.fidelity.Jfid_dtheta(g, f, h, myh)
        elif self.hmethod == 'hnonnegative_normalization':
            dh_fid = self.fidelity.Jfid_dtheta_norm(g, f, h, myh)
        elif self.hmethod == 'hnonnegative_bandlimited':
            dh_fid = self.fidelity.Jfid_dtheta_bl(g, f, h, myh)
        elif self.hmethod == 'hnonnegative_normalization_bandlimited':
            dh_fid = self.fidelity.Jfid_dtheta_norm_bl(g, f, h, myh)
        else:
            print('BDhParaMethod={} not defined in "GlobalVar"'.format(self.hmethod))
            sys.exit()

        return dh_fid


if __name__ == '__main__':
    '''
    test this class
    '''

    par = GlobalVar()
    img = Img(par)
    init = Initialization(par, img)

    imgData = img.read_image_data()

    g = init.init_g(imgData)
    f = init.init_f(par, imgData)
    h = init.init_h(par, imgData)

    fide = Fidelity(par, g)
    regu = Regularization(par)

    obj = ObjFun(par, regu, fide)

    tm_start = datetime.datetime.now()
    for i in range(0, 200):
        d0 = obj.Jall(g, np.sqrt(f), np.sqrt(h))
        df = obj.Jall_df(g, np.sqrt(f), np.sqrt(h))
        dh = obj.Jall_dh(g, np.sqrt(f), np.sqrt(h))
    tm_end = datetime.datetime.now()
    print('求200次代价函数及其导数所需时间{}'.format(tm_end - tm_start))

    # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%#
    d = np.zeros_like(df)
    d[:, :] = df[:, :]
    plt.rcParams['font.size'] = 20
    plt.subplot(1, 2, 1)
    plt.imshow(d, interpolation='nearest', cmap='bone')
    plt.colorbar()
    plt.title('dJ/df')

    d[:, :] = dh[0, :, :]
    plt.subplot(1, 2, 2)
    plt.imshow(d, interpolation='nearest', cmap='bone')
    plt.colorbar()
    plt.title('dJ/dh')

    plt.show()
