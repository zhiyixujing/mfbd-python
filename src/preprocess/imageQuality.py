import cv2
import numpy as np
import scipy.fft as fftlib
from scipy.signal import convolve2d
from scipy.signal.windows import tukey


def ImgQuality(Image):
    """
    根据不同评价标准对多帧图像进行评价并进行综合排序
    :param Image: 多帧待评价图像
    :return: ImgQlt 像质评价数组，第一列为图像原序号，第二列为综合排名（数值越小像质越好），第三到十列依次为按不同方法得到的像质评价
    """

    FrameNum = Image.shape[0]  # 帧数
    ImgQlt = np.zeros((FrameNum, 10))
    # 第一列图像序号
    # 第二列综合排名
    # 第三列Sobel梯度
    # 第四列幅度谱
    # 第五列图像平方
    # 第六列图像熵
    # 第七列香农熵
    # 第八列Fisher算子
    # 第九列过曝像素数
    # 第十列能量集中度

    for fileno in range(FrameNum):
        Imgk = Image[fileno]

        ImgQlt[fileno, 0] = fileno
        ImgQlt[fileno, 2] = Quality_SobelGrad(Imgk)
        ImgQlt[fileno, 3] = Quality_FreAmp(Imgk)
        ImgQlt[fileno, 4] = Quality_ISquare(Imgk)
        ImgQlt[fileno, 5] = Quality_ImageEntropy(Imgk)
        ImgQlt[fileno, 6] = Quality_ShannonEntropy(Imgk)
        ImgQlt[fileno, 7] = Quality_Fisher(Imgk)
        ImgQlt[fileno, 8] = Quality_OverExposure(Imgk)
        ImgQlt[fileno, 9] = Quality_EnergyCentrality(Imgk)

        # 过曝图前后三帧都认为过曝
        OverExpo = ImgQlt[:, 8]
        se = np.ones((1, 7))
        OverExpo = cv2.dilate(OverExpo, se).flatten()
        OverExpo[OverExpo > 0] = 2
        ImgQlt[:, 8] = np.maximum(ImgQlt[:, 8], OverExpo)

    # 第三列评价因子按从大到小排列
    ImgQlt = ImgQlt[ImgQlt[:, 2].argsort()[::-1], :]
    ImgQlt[:, 1] = np.arange(1, FrameNum + 1)

    # 第四列评价因子按从大到小排列
    ImgQlt = ImgQlt[ImgQlt[:, 3].argsort()[::-1], :]
    ImgQlt[:, 1] += np.arange(1, FrameNum + 1)

    # 第五列评价因子按从大到小排列
    ImgQlt = ImgQlt[ImgQlt[:, 4].argsort()[::-1], :]
    ImgQlt[:, 1] += np.arange(1, FrameNum + 1)

    # 第六列评价因子按从大到小排列
    ImgQlt = ImgQlt[ImgQlt[:, 5].argsort()[::-1], :]
    ImgQlt[:, 1] += np.arange(1, FrameNum + 1)

    # # 第七列评价因子按从大到小排列
    # ImgQlt = ImgQlt[ImgQlt[:,6].argsort()[::-1], :]
    # ImgQlt[:, 1] += range(FrameNum)

    # 第八列评价因子按从大到小排列
    ImgQlt = ImgQlt[ImgQlt[:, 7].argsort()[::-1], :]
    ImgQlt[:, 1] += np.arange(1, FrameNum + 1)

    # 第二列综合评价因子按从小到大排列
    ImgQlt = ImgQlt[ImgQlt[:, 1].argsort(kind='stable'), :]
    ImgQlt[:, 1] = np.arange(1, FrameNum + 1)

    ImgQlt = ImgQlt[ImgQlt[:, 8].argsort(kind='stable'), :]
    ImgQlt[:, 1] = np.arange(1, FrameNum + 1)

    ImgQlt = ImgQlt[ImgQlt[:, 0].argsort(), :]

    return ImgQlt[:, :2]


def Quality_SobelGrad(Image, cutoff=0.05):
    """
    根据sobel梯度计算像质
    :param Image: 待评价图像
    :param cutoff
    :return: ImageQuality像质评价值
    """
    # 图像归一化
    Image = Image / Image.sum()

    # 计算sobel梯度
    h00 = np.array([[1, 2, 1], [0, 0, 0], [-1, -2, -1]])  # 0边缘方向
    h45 = np.array([[2, 1, 0], [1, 0, -1], [0, -1, -2]])  # 45边缘方向
    h90 = np.array([[-1, 0, 1], [-2, 0, 2], [-1, 0, 1]])  # 90边缘方向
    h35 = np.array([[0, 1, 2], [-1, 0, 1], [-2, -1, 0]])  # 135边缘方向

    g00 = conv2(Image, h00, 'same')
    g45 = conv2(Image, h45, 'same')
    g90 = conv2(Image, h90, 'same')
    g35 = conv2(Image, h35, 'same')

    g1 = np.sqrt(g00 * g00 + g90 * g90)
    g2 = np.sqrt(g45 * g45 + g35 * g35)
    G = np.maximum(g1, g2)

    # 边缘加窗屏蔽，防止边缘梯度突变
    whann = np.dot(tukey(Image.shape[0], 0.05)[:, None], tukey(Image.shape[1], 0.05)[None, :])
    G *= whann

    # 输出评价结果
    num = round(np.size(Image) * cutoff)
    Gsort = np.sort(G, axis=None)[::-1]
    ImageQuality = Gsort[:num].sum() / (num + 1)
    return ImageQuality


def Quality_FreAmp(Image, cutoff=None):
    """
    根据频域幅度谱和计算像质
    :param Image: 待评价图像
    :param cutoff
    :return: ImageQuality像质评价值
    """

    H, W = Image.shape

    if cutoff is None:
        cutoff = int(np.ceil(min(H, W) / 2))

    rmax = int(np.ceil(min(H, W) / 2))

    # 加hanning窗
    whann = np.dot(tukey(H, 0.25)[:, None], tukey(W, 0.25)[None, :])
    I = Image * whann

    # 归一化
    I /= I.sum()

    # FFT
    # J = fftlib.fftshift(abs(fft2D(I)))
    J = fftlib.fftshift(abs(fftlib.fft2(I)))

    # 计算坐标
    yy, xx = np.mgrid[-int(np.ceil(W / 2)):W - int(np.ceil(W / 2)), -
                      int(np.ceil(H / 2)):H - int(np.ceil(H / 2))]
    r = np.sqrt(xx**2 + yy**2)

    Amp = np.zeros(rmax)
    for index in range(1, rmax + 1):
        circ = ((r >= index - 1) & (r < index)) * (index - r)
        Amp[index - 1] = (J * circ).sum()

    ImageQuality = Amp[9:min(rmax, cutoff)].sum()
    return ImageQuality


def Quality_ISquare(Image):
    """
    计算I平方像清晰化函数
    :param Image: 待评价图像
    :return: ImageQuality像质评价值
    """
    # 图像归一化
    Image = Image / Image.sum()

    ImageQuality = np.size(Image) * ((Image * Image).sum())
    return ImageQuality


def Quality_ImageEntropy(Image):
    """
    计算图像熵
    :param Image: 待评价图像
    :return: ImageQuality像质评价值
    """
    I = Image / Image.mean()

    # remove zero entries in p
    p = I[I != 0]

    ImageQuality = (p * np.log2(p)).sum() / np.size(Image)
    return ImageQuality


def Quality_ShannonEntropy(Image):
    """
    计算香农信息熵
    :param Image: 待评价图像
    :return: ImageQuality像质评价值
    """
    I = np.round(Image)
    I[I < 0] = 0
    I = np.int16(I)

    # calculate histogram counts
    I_hist, _ = np.histogram(I, bins=range(32768))

    # remove zero entries in p
    p = I_hist[I_hist != 0]

    p = p / np.size(Image)

    ImageQuality = -(p * np.log2(p)).sum()
    return ImageQuality


def Quality_Fisher(Image):
    """
    计算Fisher信息
    :param Image: 待评价图像
    :return: ImageQuality像质评价值
    """
    # 图像归一化
    Image = Image / Image.sum()

    p = np.sqrt(Image)
    tmpx = p[1:, :-1] - p[:-1, :-1]
    tmpy = p[:-1, 1:] - p[:-1, :-1]
    pImage = tmpx**2 + tmpy**2
    ImageQuality = pImage.sum()
    return ImageQuality


def Quality_OverExposure(Image, thrshd=65530):
    """
    计算曝光饱和像素数量
    :param Image: 待评价图像
    :param thrshd: 
    :return: ImageQuality曝光饱和像素数量
    """
    BW = np.zeros_like(Image)

    se = np.ones((3, 3))
    BW[Image > thrshd] = 1

    maker = cv2.erode(BW, se)
    BW = imreconstruct(maker, BW)

    ImageQuality = np.size(BW[BW != 0])
    return ImageQuality


def Quality_EnergyCentrality(Image, cutoff=0.05):
    """
    计算能量集中度
    :param Image: 待评价图像
    :param thrshd: 
    :return: ImageQuality曝光饱和像素数量
    """

    # 图像归一化
    Image = Image / Image.sum()

    # 确定目标区域
    thresh = Image.mean()
    mask = np.zeros_like(Image)
    mask[Image >= thresh] = 1
    se = np.ones((3, 3))
    mask = cv2.erode(mask, se)
    mask = cv2.dilate(mask, se)
    targetimg = Image * mask

    targetpixel = targetimg[targetimg != 0]
    targetpixelnum = np.size(targetpixel)

    if targetpixelnum == 0:
        ImageQuality = 0
    else:
        # 亮斑区域
        targetpixelsort = np.sort(targetpixel, axis=None)[::-1]
        thresh = targetpixelsort[max(1, int(np.ceil(targetpixelnum * cutoff)))]

        mask = np.zeros_like(Image)
        mask[Image >= thresh] = 1
        se = np.ones((5, 5))
        mask = cv2.erode(mask, se)
        mask = cv2.dilate(mask, se)
        speckleimg = Image * mask

        ImageQuality = speckleimg.sum() / targetimg.sum()
    return ImageQuality


def conv2(x, y, mode='same'):
    return np.rot90(convolve2d(np.rot90(x, 2), np.rot90(y, 2), mode=mode), 2)


def imreconstruct(marker: np.ndarray, mask: np.ndarray, radius: int = 1):
    """
    Iteratively expand the markers white keeping them limited by the mask during each iteration.
    :param marker: Grayscale image where initial seed is white on black background.
    :param mask: Grayscale mask where the valid area is white on black background.
    :param radius: Can be increased to improve expansion speed while causing decreased isolation from nearby areas.
    :returns A copy of the last expansion.
    Written By Semnodime.
    """
    kernel = np.ones(shape=(radius * 2 + 1,) * 2, dtype=np.uint8)
    while True:
        expanded = cv2.dilate(src=marker, kernel=kernel)
        cv2.bitwise_and(src1=expanded, src2=mask, dst=expanded)

        # Termination criterion: Expansion didn't change the image at all
        if (marker == expanded).all():
            return expanded
        marker = expanded
