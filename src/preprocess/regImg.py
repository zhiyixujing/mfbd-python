import numpy as np
from numpy import (arctan2, ceil, conj, fix, floor, imag, newaxis, pi, real,
                   shape, sqrt, trunc, zeros)
from scipy.fft import fft2, fftshift, ifft2, ifftshift
from scipy.ndimage import rotate
from scipy.signal import medfilt

from .imageQuality import ImgQuality


def RegImg(_par, Img):
    """
    选取像质最好的图对多帧图像进行配准，消除图像间旋转和平移
    :param Img: 待配准多帧图像
    :param _par.ImgRgdRegSel: 刚性配准参数，2：进行图像旋转+平移，1：仅进行平移，0：不配准
    :param _par.RotSmoothStat: 是否进行旋转角度中值滤波平滑，1：平滑，0：不平滑
    :param _par.RotSmoothFit: 多项式拟合旋转角度阶数，在RotSmoothStat为1时有效
    :returns

    """
    ImgRgdRegSel = _par.getInt('ImgRgdRegSel')
    RotSmoothStat = _par.getInt('RotSmoothStat')
    RotSmoothFit = _par.getInt('RotSmoothFit')

    FrameNum = Img.shape[0]  # 帧数
    ImgQlt = ImgQuality(Img)

    RegImg = np.zeros_like(Img)
    # ImgRegPara = np.zeros((FrameNum, 4))
    # # 第一列图像序号
    # # 第二列旋转角度
    # # 第三列垂直平移,trow
    # # 第四列水平平移,tcol
    # ImgRegPara[:, 0] = np.arange(1, FrameNum+1)

    # 选择该组中成像最好的顺序靠中间(10%范围内)的图为配准模板
    ImgQlt = ImgQlt[ImgQlt[:, 1].argsort(kind='stable'), :]  # 第二列综合评价因子按从小到大排列
    ImgTptSrl = ImgQlt[:round(FrameNum * 0.1) + 1, 0]
    ImgTptSrl = abs(ImgTptSrl - (1 + (FrameNum - 1) / 2))
    ImgTptIndex = ImgTptSrl.argmin()
    ImgTptIndex = int(ImgQlt[ImgTptIndex, 0])
    # ImgTptIndex = int(ImgQlt[0, 0])
    ImgTpt = Img[ImgTptIndex]

    # 旋转角度平滑
    rotang = np.zeros((FrameNum))
    if (ImgRgdRegSel == 2) & (RotSmoothStat == 1):
        # 模板前图像
        for fileno in range(ImgTptIndex - 1, -1, -1):
            ImgIndex = Img[fileno]

            subrotang = rotang[min(fileno + 1, FrameNum - 1):min(fileno + 6, FrameNum)]
            rotangindex0 = np.median(subrotang)
            rotangindexdev = subrotang.max() - subrotang.min()
            rotang[fileno] = pocregistration(
                ImgTpt, ImgIndex, rotangindex0 - max(rotangindexdev, 5), rotangindex0 + max(rotangindexdev, 5))

        # 模板后图像
        for fileno in range(ImgTptIndex + 1, FrameNum):
            ImgIndex = Img[fileno]

            subrotang = rotang[max(fileno - 5, 0):max(fileno, 1)]
            rotangindex0 = np.median(subrotang)
            rotangindexdev = subrotang.max() - subrotang.min()
            rotang[fileno] = pocregistration(
                ImgTpt, ImgIndex, rotangindex0 - max(rotangindexdev, 5), rotangindex0 + max(rotangindexdev, 5))

        # 拟合运动规律
        # 中值滤波剔除最大最小值
        rotangmed = medfilt(rotang)
        # 多项式拟合
        rotangp = np.polyfit(np.arange(FrameNum), rotangmed, RotSmoothFit)
        rotang = np.polyval(rotangp, np.arange(FrameNum))

    # 配准参数计算
    for fileno in range(FrameNum):
        ImgIndex = Img[fileno]

        if ImgRgdRegSel != 0:
            if ImgRgdRegSel == 2:  # 旋转+平移
                # 是否旋转角度平滑，不平滑直接旋转配准，平滑则在上面已经赋值
                if RotSmoothStat == 0:
                    rotang[fileno] = pocregistration(ImgTpt, ImgIndex)
            elif ImgRgdRegSel == 1:  # 仅平移
                rotang[fileno] = 0

            # 平移像素估计
            ImgTptFFT = fft2(ImgTpt)
            ImgIndexFFT = fft2(
                rotate(ImgIndex, rotang[fileno], reshape=False, order=1))
            output = dftregistration(ImgTptFFT, ImgIndexFFT, usfac=100)
            RegImg[fileno] = abs(ifft2(output[-1]))
            # ImgRegPara[fileno, 1] = rotang[fileno]
            # ImgRegPara[fileno, 2] = output[0]
            # ImgRegPara[fileno, 3] = output[1]

        else:  # 不配准
            RegImg[fileno] = ImgIndex
            # ImgRegPara[fileno, 1:] = 0

    # # 按像质从最优到最差输出配准图像
    # ImgQltRank = ImgQlt[:, 0].astype('int')

    # return RegImg[ImgQltRank]
    return RegImg


def pocregistration(img1, img2, thetamin=None, thetamax=None):
    """
    计算待配准图像相对配准模板的旋转角度
    :param Img1: 配准模板图像
    :param Img2: 待配准图像
    :param thetamin: 最小旋转角度
    :param thetamax: 最大旋转角度
    :return theta0: 配准时的旋转角度
    """

    if (thetamin is None) or (thetamax - thetamin > 360):
        thetamin = 0
    if (thetamax is None) or (thetamax - thetamin > 360):
        thetamax = 360

    intthetamin = int(np.floor(thetamin))
    intthetamax = int(np.ceil(thetamax))

    whann = np.zeros_like(img1)  # 2D Hanning window
    wgaus = np.zeros_like(img1)  # 2D Gaussian window
    M, N = img1.shape
    for m in range(M):
        for n in range(N):
            whann[m, n] = 0.25 * (1 - np.cos(2 * np.pi * m / (M - 1))) * (1 - np.cos(2 * np.pi * n / (N - 1)))
            wgaus[m, n] = np.exp(-2 * np.pi**2 * ((m - (M - 1) / 2) ** 2 + (n - (N - 1) / 2)**2) / ((M + N) / 2)**2)

    fftimg1 = fft2(img1 * whann)
    regcor = np.zeros(360)

    if intthetamax - intthetamin > 90:
        # 先以4°为采样间隔，减少计算量
        for theta in range(intthetamin, intthetamax + 1, 4):
            if regcor[np.mod(theta, 360)] != 0:
                continue
            H, W = img2.shape
            rotimg2 = rotate(img2, theta, reshape=False, order=1)
            fftimg2 = fft2(rotimg2 * whann)
            R = fftimg1 * np.conj(fftimg2)
            R = ifftshift(fftshift(R * wgaus))
            r = ifft2(R / abs(R))
            filr = abs(r)
            regcor[np.mod(theta, 360)] = filr.max()

        regcorsort = np.zeros((360, 2))
        regcorsort[:, 0] = np.arange(1, 361)
        regcorsort[:, 1] = regcor
        regcorsort = regcorsort[regcorsort[:, 1].argsort()[::-1], :]

        # 查找4°采样峰值，在峰值[-5,+5]附近以1°间隔采样
        for i in range(3):
            theta0 = int(regcorsort[i, 0])
            for theta in range(theta0 - 3, theta0 + 4):
                if regcor[np.mod(theta, 360)] != 0:
                    continue
                rotimg2 = rotate(img2, theta, reshape=False, order=1)
                fftimg2 = fft2(rotimg2 * whann)
                R = fftimg1 * np.conj(fftimg2)
                R = ifftshift(fftshift(R * wgaus))
                r = ifft2(R / abs(R))
                filr = abs(r)
                regcor[np.mod(theta, 360)] = filr.max()

    else:
        # 以1°间隔采样
        for theta in range(intthetamin, intthetamax + 1):
            if regcor[np.mod(theta, 360)] != 0:
                continue
            rotimg2 = rotate(img2, theta, reshape=False, order=1)
            fftimg2 = fft2(rotimg2 * whann)
            R = fftimg1 * np.conj(fftimg2)
            R = ifftshift(fftshift(R * wgaus))
            r = ifft2(R / abs(R))
            filr = abs(r)
            regcor[np.mod(theta, 360)] = filr.max()

    # 得到峰值的范围在thetaL和thetaR之间，thetaL和thetaR范围为[0 360)
    regcor0 = regcor.max()
    theta0 = regcor.argmax()

    Lsetp = 0.5

    k = 8
    while k > 0:
        k -= 1
        thetaL = np.mod(theta0 - Lsetp, 360)
        rotimg2 = rotate(img2, thetaL, reshape=False, order=1)
        fftimg2 = fft2(rotimg2 * whann)
        R = fftimg1 * np.conj(fftimg2)
        R = ifftshift(fftshift(R * wgaus))
        r = ifft2(R / abs(R))
        filr = abs(r)
        regcorL = filr.max()

        thetaR = np.mod(theta0 + Lsetp, 360)
        rotimg2 = rotate(img2, thetaR, reshape=False, order=1)
        fftimg2 = fft2(rotimg2 * whann)
        R = fftimg1 * np.conj(fftimg2)
        R = ifftshift(fftshift(R * wgaus))
        r = ifft2(R / abs(R))
        filr = abs(r)
        regcorR = filr.max()

        if (regcor0 >= regcorL) & (regcor0 >= regcorR):
            Lsetp /= 2
        else:
            if regcorL > regcorR:
                theta0 = thetaL
                regcor0 = regcorL
            elif regcorL < regcorR:
                theta0 = thetaR
                regcor0 = regcorR
            else:
                Lsetp /= 2

    # 得到峰值为theta0，为[-180 180)
    if theta0 >= 180:
        theta0 -= 360

    return theta0


def dftregistration(buf1ft, buf2ft, usfac=1, return_registered=True, maxoff=None):
    """
    translated from matlab:
    http://www.mathworks.com/matlabcentral/fileexchange/18401-efficient-subpixel-image-registration-by-cross-correlation/content/html/efficient_subpixel_registration.html

    Efficient subpixel image registration by crosscorrelation. This code
    gives the same precision as the FFT upsampled cross correlation in a
    small fraction of the computation time and with reduced memory
    requirements. It obtains an initial estimate of the crosscorrelation peak
    by an FFT and then refines the shift estimation by upsampling the DFT
    only in a small neighborhood of that estimate by means of a
    matrix-multiply DFT. With this procedure all the image points are used to
    compute the upsampled crosscorrelation.
    Manuel Guizar - Dec 13, 2007

    Portions of this code were taken from code written by Ann M. Kowalczyk
    and James R. Fienup.
    J.R. Fienup and A.M. Kowalczyk, "Phase retrieval for a complex-valued
    object by using a low-resolution image," J. Opt. Soc. Am. A 7, 450-458
    (1990).

    Citation for this algorithm:
    Manuel Guizar-Sicairos, Samuel T. Thurman, and James R. Fienup,
    "Efficient subpixel image registration algorithms," Opt. Lett. 33,
    156-158 (2008).

    Inputs
    buf1ft    Fourier transform of reference image,
           DC in (1,1)   [DO NOT FFTSHIFT]
    buf2ft    Fourier transform of image to register,
           DC in (1,1) [DO NOT FFTSHIFT]
    usfac     Upsampling factor (integer). Images will be registered to
           within 1/usfac of a pixel. For example usfac = 20 means the
           images will be registered within 1/20 of a pixel. (default = 1)

    Outputs
    output =  [error,diffphase,net_row_shift,net_col_shift]
    error     Translation invariant normalized RMS error between f and g
    diffphase     Global phase difference between the two images (should be
               zero if images are non-negative).
    net_row_shift net_col_shift   Pixel shifts between images
    Greg      (Optional) Fourier transform of registered version of buf2ft,
           the global phase difference is compensated for.
    """

    # Compute error for no pixel shift
    if usfac == 0:
        raise ValueError("Upsample Factor must be >= 1")
        CCmax = sum(sum(buf1ft * conj(buf2ft)))
        rfzero = sum(abs(buf1ft)**2)
        rgzero = sum(abs(buf2ft)**2)
        error = 1.0 - CCmax * conj(CCmax) / (rgzero * rfzero)
        error = sqrt(abs(error))
        diffphase = arctan2(imag(CCmax), real(CCmax))
        output = [error, diffphase]

    # Whole-pixel shift - Compute crosscorrelation by an IFFT and locate the
    # peak
    elif usfac == 1:
        [m, n] = shape(buf1ft)
        CC = ifft2(buf1ft * conj(buf2ft))
        if maxoff is None:
            rloc, cloc = np.unravel_index(abs(CC).argmax(), CC.shape)
            CCmax = CC[rloc, cloc]
        else:
            # set the interior of the shifted array to zero
            # (i.e., ignore it)
            CC[maxoff:-maxoff, :] = 0
            CC[:, maxoff:-maxoff] = 0
            rloc, cloc = np.unravel_index(abs(CC).argmax(), CC.shape)
            CCmax = CC[rloc, cloc]
        rfzero = sum(abs(buf1ft)**2) / (m * n)
        rgzero = sum(abs(buf2ft)**2) / (m * n)
        error = 1.0 - CCmax * conj(CCmax) / (rgzero * rfzero)
        error = sqrt(abs(error))
        diffphase = arctan2(imag(CCmax), real(CCmax))
        md2 = fix(m / 2)
        nd2 = fix(n / 2)
        if rloc > md2:
            row_shift = rloc - m
        else:
            row_shift = rloc

        if cloc > nd2:
            col_shift = cloc - n
        else:
            col_shift = cloc
        output = [row_shift, col_shift]

    # Partial-pixel shift
    else:

        # First upsample by a factor of 2 to obtain initial estimate
        # Embed Fourier data in a 2x larger array
        [m, n] = shape(buf1ft)
        mlarge = m * 2
        nlarge = n * 2
        CClarge = zeros([mlarge, nlarge], dtype='complex')
        CClarge[int(m - np.fix(m / 2)):int(m + np.fix((m - 1) / 2) + 1), int(n - np.fix(n / 2)):int(n + np.fix((n - 1) / 2) + 1)] = fftshift(buf1ft) * conj(fftshift(buf2ft))
        # note that matlab uses fix which is trunc... ?

        # Compute crosscorrelation and locate the peak
        CC = ifft2(ifftshift(CClarge))  # Calculate cross-correlation
        if maxoff is None:
            rloc, cloc = np.unravel_index(abs(CC).argmax(), CC.shape)
            CCmax = CC[rloc, cloc]
        else:
            # set the interior of the shifted array to zero
            # (i.e., ignore it)
            CC[maxoff:-maxoff, :] = 0
            CC[:, maxoff:-maxoff] = 0
            rloc, cloc = np.unravel_index(abs(CC).argmax(), CC.shape)
            CCmax = CC[rloc, cloc]

        # Obtain shift in original pixel grid from the position of the
        # crosscorrelation peak
        [m, n] = shape(CC)
        md2 = trunc(m / 2)
        nd2 = trunc(n / 2)
        if rloc > md2:
            row_shift2 = rloc - m
        else:
            row_shift2 = rloc
        if cloc > nd2:
            col_shift2 = cloc - n
        else:
            col_shift2 = cloc
        row_shift2 = row_shift2 / 2.
        col_shift2 = col_shift2 / 2.

        # If upsampling > 2, then refine estimate with matrix multiply DFT
        if usfac > 2:
            # DFT computation
            # Initial shift estimate in upsampled grid
            zoom_factor = 1.5
            row_shift0 = round(row_shift2 * usfac) / usfac
            col_shift0 = round(col_shift2 * usfac) / usfac
            # Center of output array at dftshift+1
            dftshift = trunc(ceil(usfac * zoom_factor) / 2)

            # Matrix multiply DFT around the current shift estimate
            roff = dftshift - row_shift0 * usfac
            coff = dftshift - col_shift0 * usfac
            upsampled = dftups(
                (buf2ft * conj(buf1ft)),
                ceil(usfac * zoom_factor),
                ceil(usfac * zoom_factor),
                usfac,
                roff,
                coff)

            CC = conj(upsampled) / (md2 * nd2 * usfac**2)

            # Locate maximum and map back to original pixel grid
            rloc, cloc = np.unravel_index(abs(CC).argmax(), CC.shape)
            rloc0, cloc0 = np.unravel_index(abs(CC).argmax(), CC.shape)
            CCmax = CC[rloc, cloc]

            rg00 = dftups(buf1ft * conj(buf1ft), 1, 1, usfac) / \
                (md2 * nd2 * usfac**2)
            rf00 = dftups(buf2ft * conj(buf2ft), 1, 1, usfac) / \
                (md2 * nd2 * usfac**2)

            rloc = rloc - dftshift  # + 1 # +1 # questionable/failed hack + 1;
            cloc = cloc - dftshift  # + 1 # -1 # questionable/failed hack - 1;

            row_shift = row_shift0 + rloc / usfac
            col_shift = col_shift0 + cloc / usfac

        # If upsampling = 2, no additional pixel shift refinement
        else:
            rg00 = sum(sum(buf1ft * conj(buf1ft))) / m / n
            rf00 = sum(sum(buf2ft * conj(buf2ft))) / m / n
            row_shift = row_shift2
            col_shift = col_shift2
        error = 1.0 - CCmax * conj(CCmax) / (rg00 * rf00)
        error = sqrt(abs(error))
        diffphase = arctan2(imag(CCmax), real(CCmax))
        # If its only one row or column the shift along that dimension has no effect. We set to zero.
        if md2 == 1:
            row_shift = 0
        if nd2 == 1:
            col_shift = 0
        output = [row_shift, col_shift]

    # Compute registered version of buf2ft
    if (return_registered):
        if (usfac > 0):
            nr, nc = shape(buf2ft)
            Nr = ifftshift(
                np.linspace(-np.fix(nr / 2), np.ceil(nr / 2) - 1, nr))
            Nc = ifftshift(
                np.linspace(-np.fix(nc / 2), np.ceil(nc / 2) - 1, nc))
            [Nc, Nr] = np.meshgrid(Nc, Nr)
            Greg = buf2ft * \
                np.exp(1j * 2 * np.pi * (-row_shift * Nr / nr - col_shift * Nc / nc))
            Greg = Greg * np.exp(1j * diffphase)
        elif (usfac == 0):
            Greg = buf2ft * np.exp(1j * diffphase)
        output.append(Greg)

    return output


def dftups(inp, nor=None, noc=None, usfac=1, roff=0, coff=0):
    """
    Translated from matlab:

     * `Original Source <http://www.mathworks.com/matlabcentral/fileexchange/18401-efficient-subpixel-image-registration-by-cross-correlation/content/html/efficient_subpixel_registration.html>`_
     * Manuel Guizar - Dec 13, 2007
     * Modified from dftus, by J.R. Fienup 7/31/06

    Upsampled DFT by matrix multiplies, can compute an upsampled DFT in just a small region.

    This code is intended to provide the same result as if the following
    operations were performed:

      * Embed the array "in" in an array that is usfac times larger in each
        dimension. ifftshift to bring the center of the image to (1,1).
      * Take the FFT of the larger array
      * Extract an [nor, noc] region of the result. Starting with the
        [roff+1 coff+1] element.

    It achieves this result by computing the DFT in the output array without
    the need to zeropad. Much faster and memory efficient than the
    zero-padded FFT approach if [nor noc] are much smaller than [nr*usfac nc*usfac]

    Parameters
    ----------
    usfac : int
        Upsampling factor (default usfac = 1)
    nor,noc : int,int
        Number of pixels in the output upsampled DFT, in units of upsampled
        pixels (default = size(in))
    roff, coff : int, int
        Row and column offsets, allow to shift the output array to a region of
        interest on the DFT (default = 0)
    """

    nr, nc = np.shape(inp)
    # Set defaults
    if noc is None:
        noc = nc
    if nor is None:
        nor = nr
    # Compute kernels and obtain DFT by matrix products
    term1c = (ifftshift(np.arange(nc, dtype='float') -
              floor(nc / 2)).T[:, newaxis]) / nc  # fftfreq
    term2c = ((np.arange(noc, dtype='float') - coff) /
              usfac)[newaxis, :]              # output points
    kernc = np.exp((-1j * 2 * pi) * term1c * term2c)

    term1r = (np.arange(nor, dtype='float').T -
              roff)[:, newaxis]                # output points
    term2r = (ifftshift(np.arange(nr, dtype='float')) -
              floor(nr / 2))[newaxis, :]  # fftfreq
    kernr = np.exp((-1j * 2 * pi / (nr * usfac)) * term1r * term2r)

    out = np.dot(np.dot(kernr, inp), kernc)
    return out
