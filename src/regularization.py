import sys

import matplotlib.pyplot as plt
import numpy as np

from globalvar import GlobalVar
from Img import Img
from initialization import Initialization
from mpiAdapter import MPIAdapter


class Regularization:
    """
    处理正则化的类，正则化只和真实图像 f 有关, 与 h 和 g 无关
    处理正则化项的代价函数 Jreg(self,f)， 或者代价函数的导数 dJ/df, dJ/dphi
    """

    def __init__(self, _par):
        '''
        :param _par:   # 控制输入参数的类
        :param _g；    # 观察图像，g 不随迭代而改变，实际上g也可以不用，因为正则项仅仅与 f 有关
        '''

        self.par = _par
        self.fmethod = self.par.getString("BDfRegularMethod").lower()

    def Jreg(self, f):
        '''
        正则化约束，它仅是真实图像的函数，而且真实图像是 1 帧的
        :param: f 真实的图像，它是一个[height,width] 2D复数矩阵，因为在FFT是在复数域进行的。或者 np.real(f), np.abs(f)
        :return: 标量，代价函数的正则化部分
        '''

        ff = abs(f)        # 变成实数

        rglr = 0
        if self.fmethod == 'none':
            rglr = 0

        elif self.fmethod == 'tikhonov':
            coef = self.par.getFloat("BDfRegularCoef")
            rglr = np.mean(ff * ff) * coef

        elif self.fmethod == 'tv':
            # diff(ff,axis) 沿着某个给定的轴进行差分
            # padarray():填充数组
            diff1 = np.pad(np.diff(ff, axis=0), ((0, 1), (0, 0)), 'constant')
            diff2 = np.pad(np.diff(ff, axis=1), ((0, 0), (0, 1)), 'constant')
            coef = self.par.getFloat("BDfRegularCoef")
            beta = self.par.getFloat("BDfRegularTVIndex")
            rglr = np.mean(np.power(diff1 * diff1 + diff2 * diff2, 0.5 * beta)) * coef
        else:
            print('BDfRegularMethod={} not defined in "GlobalVar"'.format(self.fmethod))
            sys.exit()
        return MPIAdapter.avage(rglr)

    def Jreg_df(self, f):
        '''
        正则化约束的导数项 dJ/df, dJ/ds = dJ/df df/ds
        :param: f 真实的图像，它是一个[height,width] 2D复数矩阵，因为在FFT是在复数域进行的。或者 np.real(f), np.abs(f)
        :return: [height,width] 2D实数矩阵，正则化项对f的导数
        '''

        ff = np.real(f)        # 变成实数
        eps = np.finfo(np.float32).eps

        dreg = np.zeros(shape=(f.shape[0], f.shape[1]), dtype=float)
        if self.fmethod == 'none':
            dreg = 0

        elif self.fmethod == 'tikhonov':
            coef = self.par.getFloat("BDfRegularCoef")
            dreg = ff * (2 * coef)

        elif self.fmethod == 'tv':
            # diff(ff,axis) 沿着某个给定的轴进行差分
            # padarray():填充数组
            diff1 = np.pad(np.diff(ff, axis=0), ((0, 1), (0, 0)), 'constant')
            diff2 = np.pad(np.diff(ff, axis=1), ((0, 0), (0, 1)), 'constant')
            coef = self.par.getFloat("BDfRegularCoef")
            beta = self.par.getFloat("BDfRegularTVIndex")
            diff0 = np.power(diff1 * diff1 + diff2 * diff2, 1.0 - 0.5 * beta) + eps

            out1 = np.pad(np.diff(diff1 / diff0, axis=0), ((1, 0), (0, 0)), 'constant')
            out2 = np.pad(np.diff(diff2 / diff0, axis=1), ((0, 0), (1, 0)), 'constant')
            dreg = (-coef * beta) * (out1 + out2)
        else:
            print('BDfRegularMethod={} not defined in "GlobalVar"'.format(self.fmethod))
            sys.exit()
        return dreg

    def Jreg_dphi(self, f, phi):
        '''
        正则化约束的导数项 d{J}/d{phi}, 其中 f= phi^2, 则 dJ/dphi= dJ/df *(2*phi)
        :param: phi 真实的图像，它是一个[height,width] 2D复数矩阵，或者 np.real(f), np.abs(f)
        :return: [height,width] 2D实数矩阵，正则化项对 phi的导数
        '''

        return self.Jreg_df(f) * (2 * phi)


if __name__ == '__main__':
    '''
    test this class
    '''

    par = GlobalVar()
    img = Img(par)
    init = Initialization(par, img)

    imgData = img.read_image_data()

    f = init.init_f(par, imgData)

    regular = Regularization(par)

    for i in range(0, 100):
        reg = regular.Jreg(f)
        df = regular.Jreg_df(f)
        dphi = regular.Jreg_dphi(f)

    print(reg)
